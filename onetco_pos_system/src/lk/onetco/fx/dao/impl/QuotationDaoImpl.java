package lk.onetco.fx.dao.impl;

import lk.onetco.fx.dao.QuotationDao;
import lk.onetco.fx.database.DBConnection;
import lk.onetco.fx.model.Quotation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class QuotationDaoImpl implements QuotationDao {
    @Override
    public boolean addQuotaion(Quotation quotation) throws SQLException {
        Connection connection = null;
        try {
            connection= DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement pstm = connection.prepareStatement("INSERT INTO quotation VALUES(?,?,?,?,?,?)");
        pstm.setObject(1,quotation.getQuotationId());
        pstm.setObject(2,quotation.getQuotationCode());
        pstm.setObject(3,quotation.getItemName());
        pstm.setObject(4,quotation.getItemQuantity());
        pstm.setObject(5,quotation.getItemPrice());
        pstm.setObject(6,quotation.getQuotationDate());
        return pstm.executeUpdate()>0;
    }

    @Override
    public int getNextQuotationId() throws SQLException {
        Connection connection = null;
        int nextQuotationId=0;
        try {
            connection= DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement ps = connection.prepareStatement("select max(quotation_id) from quotation");
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {

            nextQuotationId= rset.getInt(1)+1;

        }
        return nextQuotationId;
    }

    public static String generateQuotationCode() throws SQLException{
        Connection connection = null;
        String quotationCode="QUO_";
        int maxQuotationId=0;
        int nextId;
        try {
            connection= DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement ps = connection.prepareStatement("select max(quotation_id) from quotation");
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            maxQuotationId= rset.getInt(1);
        }
        nextId=++maxQuotationId;
        quotationCode=quotationCode+nextId;
        return quotationCode;
    }
}
