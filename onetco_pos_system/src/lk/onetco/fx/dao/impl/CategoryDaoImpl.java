package lk.onetco.fx.dao.impl;

import lk.onetco.fx.dao.CategoryDao;
import lk.onetco.fx.database.DBConnection;
import lk.onetco.fx.model.Category;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CategoryDaoImpl implements CategoryDao {

    @Override
    public boolean addCategory(Category category) throws SQLException {
        Connection connection = null;
        try {
            connection = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement pstm = connection.prepareStatement("INSERT INTO category VALUES (?,?,?)");
       // String maxCategoryCode=getNextCategoryCode();
        pstm.setObject(1, category.getCategoryId());
        pstm.setObject(2, category.getCategoryCode());
        pstm.setObject(3, category.getCategoryName());

        return pstm.executeUpdate() > 0;
    }

    @Override
    public boolean deleteCategory(int id) throws SQLException {
        String Query = "DELETE FROM category WHERE category_id = ? ";
        Connection connection = null;
        try {
            connection = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }

        PreparedStatement pstm = connection.prepareStatement(Query);
        pstm.setObject(1, id);

        pstm.executeUpdate();

        return false;
    }

    @Override
    public boolean updateCategory(Category category) throws SQLException {

        String Query = "UPDATE category SET category_code = ? , category_name = ? WHERE category_id = ? ";
        Connection connection = null;
        try {
            connection = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }

        PreparedStatement pstm = connection.prepareStatement(Query);
        pstm.setObject(1, category.getCategoryCode());
        pstm.setObject(2, category.getCategoryName());
        pstm.setObject(3, category.getCategoryId());

        pstm.executeUpdate();


        return false;
    }

    @Override
    public ResultSet getAllCategory() throws SQLException {
        String query="SELECT category_code,category_name FROM category";
        Connection con = null;
        try {
            con = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement ps = con.prepareStatement(query);
        ResultSet rst = ps.executeQuery();
        return rst;
    }

    @Override
    public String getCategoryNameByCategoryCode(String categoryCode) throws SQLException {
        String query="SELECT category_name FROM category WHERE category_id=?";
        Connection connection=null;
        try {
          connection=DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement pst = connection.prepareStatement(query);
        pst.setObject(1,categoryCode);
        ResultSet rst = pst.executeQuery();
        String categoryName=null;
        if(rst.next()){ rst.first();
           categoryName=rst.getString("category_name");
        }
        return  categoryName;
    }

    public static int getNextCategoryCode() throws SQLException{
        Connection con = null;
        int maxCatID=0;
        try {
            con = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement ps = con.prepareStatement("select max(category_id) from category");
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            maxCatID = rset.getInt(1)+1;
        }
        return maxCatID;
    }
}
