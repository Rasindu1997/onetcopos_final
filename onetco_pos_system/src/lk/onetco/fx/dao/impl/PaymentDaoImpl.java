package lk.onetco.fx.dao.impl;

import lk.onetco.fx.dao.PaymentDao;
import lk.onetco.fx.database.DBConnection;
import lk.onetco.fx.model.Payment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PaymentDaoImpl implements PaymentDao {

    @Override
    public boolean addPayment(Payment payment) throws SQLException {
        Connection connection = null;
        try {
            connection= DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement pstm = connection.prepareStatement("INSERT INTO payment VALUES(?,?,?,?,?,?,?)");
        pstm.setObject(1,payment.getPaymentId());
        pstm.setObject(2,payment.getPaymentInvoiceId());
        pstm.setObject(3,payment.getPaymentCustomerId());
        pstm.setObject(4,payment.getPaymentTotal());
        pstm.setObject(5,payment.getPaymentDiscountedPrice());
        pstm.setObject(6,payment.getPaymentBalance());
        pstm.setObject(7,payment.getPaymentRemark());
        return pstm.executeUpdate()>0;
    }
}
