package lk.onetco.fx.dao.impl;

import lk.onetco.fx.dao.UserDao;
import lk.onetco.fx.database.DBConnection;
import lk.onetco.fx.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDaoImpl implements UserDao {
    @Override
    public boolean addUser(User user) throws SQLException {
        Connection connection = null;
        try {
            connection = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement pstm = connection.prepareStatement("INSERT INTO user VALUES (?,?,?,?,?)");
        pstm.setObject(1, user.getUserid());
        pstm.setObject(2, user.getUsername());
        pstm.setObject(3, user.getPassword());
        pstm.setObject(4, user.getUserType());
        pstm.setObject(5, user.getUserPasswordStatus());
        return pstm.executeUpdate()>0;

    }

    @Override
    public boolean deleteUser(int userid) throws SQLException {

        String Query = "DELETE FROM user WHERE userid = ? ";
        Connection connection = null;
        try {
            connection = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }

        PreparedStatement pstm = connection.prepareStatement(Query);
        pstm.setObject(1, userid);

        pstm.executeUpdate();

        return false;
    }

    @Override
    public boolean updateUser(User user) throws SQLException {

        String Query = "UPDATE user SET username = ? , password = ? , usertype = ? WHERE userid = ? ";
        Connection connection = null;
        try {
            connection = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }

        PreparedStatement pstm = connection.prepareStatement(Query);
        pstm.setObject(1, user.getUsername());
        pstm.setObject(2, user.getPassword());
        pstm.setObject(3, user.getUserType());
        pstm.setObject(4, user.getUserid());

        pstm.executeUpdate();
        return false;
    }

    @Override
    public boolean updateUserPassword(String password,int userId) throws SQLException {
        String Query = "UPDATE user SET  password = ?,user_password_status=? WHERE userid = ? ";
        Connection connection = null;
        try {
            connection = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }

        PreparedStatement pstm = connection.prepareStatement(Query);
        pstm.setObject(1, password);
        pstm.setObject(2, User.logged);
        pstm.setObject(3, userId);
        return pstm.executeUpdate()>0;
    }

    @Override
    public boolean userLogin(String username, String password) throws SQLException {

        Connection connection = null;
        try {
            connection = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement pstm = connection.prepareStatement("SELECT * FROM user WHERE username = ? and password = ?");
        pstm.setObject(1, username);
        pstm.setObject(2, password);

        //System.out.println(pstm);

        ResultSet resultSet = pstm.executeQuery();
        if (resultSet.next()) {
            return true;
        }

        return false;
    }

    @Override
    public ResultSet getAllUsers() throws SQLException {
        return null;
    }

    @Override
    public ResultSet getUserDetailsByUserName(String userName) throws SQLException {
        Connection conn=null;
        try{
            conn=DBConnection.getConnection();
        }catch (Exception e){
            e.printStackTrace();
        }
        PreparedStatement ps = conn.prepareStatement("select userid, username, password, usertype, user_password_status from user where username=?");
        ps.setObject(1,userName);
        ResultSet rst=ps.executeQuery();
        return rst;
    }

    public static int getNextUserId() throws SQLException{
        Connection con = null;
        int nextUserId=0;
        try {
            con = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement ps = con.prepareStatement("select max(userid) from user");
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            nextUserId = rset.getInt(1)+1;
        }
        return nextUserId;
    }


    public static boolean isCustomerNameExist(String username) throws SQLException {

        Connection connection = null;
        try {
            connection = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement pstm = connection.prepareStatement("SELECT userid FROM user WHERE username = ?");
        pstm.setObject(1,username);
        ResultSet rst=pstm.executeQuery();
        return rst.first();
    }

    public static boolean isFirstLogin(String username) throws SQLException {
        Connection connection = null;
        try {
            connection = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement pstm = connection.prepareStatement("SELECT userid FROM user WHERE user_password_status = ? and username=?");
        pstm.setObject(1,User.notLogged);
        pstm.setObject(2,username);
        ResultSet rst=pstm.executeQuery();
        return rst.first();
    }

    public static int getUserID(String username) throws SQLException{
        Connection connection = null;
        int userid=0;
        try {
            connection = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement pstm = connection.prepareStatement("SELECT userid FROM user WHERE username=?");
        pstm.setObject(1,username);
        ResultSet rst=pstm.executeQuery();
        if(rst.next()){
            rst.first();
             userid=rst.getInt("userid");
        }
        return userid;
    }
}
