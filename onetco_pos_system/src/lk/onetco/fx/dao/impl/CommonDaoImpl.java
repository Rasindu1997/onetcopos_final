package lk.onetco.fx.dao.impl;

import lk.onetco.fx.dao.CommonDao;
import lk.onetco.fx.database.DBConnection;

import java.math.BigDecimal;
import java.sql.*;
import java.time.LocalDate;

public class CommonDaoImpl implements CommonDao {
    @Override
    public int getInvoiceCount() throws SQLException {
        int invoiceCount=0;
        String Query = "SELECT COUNT(invoice_id)  AS invoice_count FROM invoice";
        Connection connection = null;
        try {
            connection = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement pstm = connection.prepareStatement(Query);
        ResultSet resultSet=pstm.executeQuery();
        if(resultSet.next()){
             resultSet.first();
             invoiceCount=resultSet.getInt("invoice_count");
        }
        return invoiceCount;
    }

    @Override
    public int getItemCount() throws SQLException {
        int itemCount=0;
        String Query = "SELECT COUNT(item_id) AS item_count FROM item";
        Connection connection = null;
        try {
            connection = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement pstm = connection.prepareStatement(Query);
        ResultSet resultSet=pstm.executeQuery();
        if(resultSet.next()){
            resultSet.first();
            itemCount=resultSet.getInt("item_count");
        }
        return itemCount;
    }

    @Override
    public int getCustomersCount() throws SQLException {
        int customersCount=0;
        String Query = "SELECT COUNT(customer_id) AS customer_count FROM customer";
        Connection connection = null;
        try {
            connection = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement pstm = connection.prepareStatement(Query);
        ResultSet resultSet=pstm.executeQuery();
        if(resultSet.next()){
            resultSet.first();
            customersCount=resultSet.getInt("customer_count");
        }
        return customersCount;
    }

    @Override
    public BigDecimal getIncome() throws SQLException {
        BigDecimal totalIncome=BigDecimal.ZERO;
        String Query = "SELECT SUM(invoice_total_price) AS invoice_sum FROM invoice";
        Connection connection = null;
        try {
            connection = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement pstm = connection.prepareStatement(Query);
        ResultSet resultSet=pstm.executeQuery();
        if(resultSet.next()){
            resultSet.first();
            totalIncome=resultSet.getBigDecimal("invoice_sum");
        }
        if(totalIncome==null){
            totalIncome=BigDecimal.ZERO;
        }
        return totalIncome;
    }

    @Override
    public ResultSet getTodayInvoices() throws SQLException {
        LocalDate l_date = LocalDate.now();
        String Query = "SELECT * FROM invoice\n" +
                "JOIN invoice_details ON invoice_id=invoice_details_invoice_id\n" +
                "JOIN payment ON invoice_id=payment_invoice_id \n" +
                "JOIN customer ON payment_customer_id=customer_id\n" +
                "WHERE cast(invoice_details_date as date)=? GROUP BY invoice_id";
        Connection connection = null;
        try {
            connection = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement pst = connection.prepareStatement(Query);
        pst.setObject(1,l_date);
        ResultSet rst=pst.executeQuery();
        return rst;
    }
}
