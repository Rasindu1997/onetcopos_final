package lk.onetco.fx.dao.impl;

import lk.onetco.fx.dao.ItemDao;
import lk.onetco.fx.database.DBConnection;
import lk.onetco.fx.model.Item;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ItemDaoImpl implements ItemDao {
    @Override
    public boolean addItem(Item item) throws SQLException {
      Connection connection = null;
        try {
            connection=DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement pstm = connection.prepareStatement("INSERT INTO item VALUES (?,?,?,?,?,?,?)");
        String maxItemCode=getNextItemCode();
        pstm.setObject(1, item.getItemId());
        pstm.setObject(2, getNextItemCode());
        pstm.setObject(3, item.getItemCategoryCode());
        pstm.setObject(4, item.getItemName());
        pstm.setObject(5, item.getRetailPrice());
        pstm.setObject(6, item.getItemStockQty());
        pstm.setObject(7, item.getItemWarranty());
        return pstm.executeUpdate() > 0;
    }

    @Override
    public boolean deleteItem(int id) throws SQLException {
        String query="DELETE FROM item WHERE item_id = ?";
        Connection con=null;
        try {
             con = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement ps = con.prepareStatement(query);
        ps.setObject(1,id);
        return ps.executeUpdate()>0;
    }

    @Override
    public boolean updateItem(Item item) throws SQLException {
        Connection connection = null;
        try {
            connection = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement pstm = connection.prepareStatement("UPDATE Item SET item_category_id=?,item_name=?,item_retail_price=?,item_stock_qty=?,item_warranty=? WHERE item_id=?");
        pstm.setObject(1, item.getItemCategoryCode());
        pstm.setObject(2, item.getItemName());
        pstm.setObject(3, item.getRetailPrice());
        pstm.setObject(4, item.getItemStockQty());
        pstm.setObject(5, item.getItemWarranty());
        pstm.setObject(6, item.getItemId());
        return pstm.executeUpdate() > 0;
    }

    @Override
    public ResultSet getAllItems() throws SQLException {
       String query="SELECT item_id,item_code,item_category_id,item_name,item_retail_price,item_stock_qty,item_warranty FROM item";
        Connection con = null;
        try {
            con = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement ps = con.prepareStatement(query);
        ResultSet rst = ps.executeQuery();
        return rst;
    }

    @Override
    public ResultSet findItems(String itemName) throws SQLException {
       String searchValue="'"+itemName+"%'";
        String query="SELECT item_id,item_code,item_category_id,item_name,item_retail_price,item_stock_qty FROM item WHERE item_name LIKE "+searchValue;
        Connection con = null;
        try {
            con = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement ps = con.prepareStatement(query);
        ResultSet rst = ps.executeQuery();
        return rst;
    }

    @Override
    public boolean reduceItemQty(int itemId, int invoicedQty) throws SQLException {
        Connection con=null;
        try {
            con = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement pstm=con.prepareStatement("UPDATE item SET item_stock_qty=item_stock_qty - "+Integer.valueOf(invoicedQty)+"  WHERE item_id=?");
        pstm.setObject(1,itemId);
        return pstm.executeUpdate()>0;
    }

    @Override
    public boolean increaseItemQty(int itemId, int qty) throws SQLException {
        Connection con=null;
        try {
            con = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement pstm=con.prepareStatement("UPDATE item SET item_stock_qty=item_stock_qty + "+Integer.valueOf(qty)+"  WHERE item_id=?");
        pstm.setObject(1,itemId);
        return pstm.executeUpdate()>0;
    }


    public static String getNextItemCode() throws SQLException{
        Connection con = null;
        String maxCourseID="ITM_";
        try {
            con = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement ps = con.prepareStatement("select max(item_id) from item");
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            maxCourseID = maxCourseID.concat(Integer.toString(rset.getInt(1)));
        }
        return maxCourseID;
    }
}
