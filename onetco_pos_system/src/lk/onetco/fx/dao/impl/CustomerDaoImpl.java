package lk.onetco.fx.dao.impl;

import lk.onetco.fx.dao.CustomerDao;
import lk.onetco.fx.database.DBConnection;
import lk.onetco.fx.model.Customer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CustomerDaoImpl implements CustomerDao {
    @Override
    public boolean addCustomer(Customer customer) throws SQLException {
        Connection connection = null;
        try {
            connection= DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement pstm = connection.prepareStatement("INSERT INTO customer VALUES(?,?,?,?)");
        pstm.setObject(1,customer.getCustomerId());
        pstm.setObject(2,customer.getFirstname());
        pstm.setObject(3,customer.getLastname());
        pstm.setObject(4,customer.getContactNo());
        return pstm.executeUpdate()>0;
    }

    @Override
    public int getNextCustomerId() throws SQLException {
        Connection connection = null;
        int maxCustomerId=0;
        try {
            connection= DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement ps = connection.prepareStatement("select max(customer_id) from customer");
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {

            maxCustomerId= rset.getInt(1);

        }
        return ++maxCustomerId;
    }
}
