package lk.onetco.fx.dao.impl;

import lk.onetco.fx.dao.InvoiceDetailsDao;
import lk.onetco.fx.database.DBConnection;
import lk.onetco.fx.model.InvoiceDetails;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class InvoiceDetailsDaoImpl implements InvoiceDetailsDao {
    @Override
    public boolean addInvoiceDetails(InvoiceDetails invoiceDetails) throws SQLException {
        Connection connection = null;
        try {
            connection= DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement pstm = connection.prepareStatement("INSERT INTO invoice_details VALUES(?,?,?,?,?,?,?,?)");
        pstm.setObject(1,invoiceDetails.getInvoiceDetailsId());
        pstm.setObject(2,invoiceDetails.getInvoiceId());
        pstm.setObject(3,invoiceDetails.getInvoiceDetailsItemId());
        pstm.setObject(4,invoiceDetails.getInvoiceDetailsItemName());
        pstm.setObject(5,invoiceDetails.getInvoiceDetailsItemPrice());
        pstm.setObject(6,invoiceDetails.getInvoiceDetailsDate());
        pstm.setObject(7,invoiceDetails.getInvoiceDetialsItemQty());
        pstm.setObject(8,invoiceDetails.getInvoiceDetailsItemDiscount());
        return  pstm.executeUpdate()>0;
    }

    @Override
    public ResultSet getInvoiceDetailsByInvoiceId(int invoiceId) throws SQLException {
        String query="SELECT invoice_details_id, invoice_details_invoice_id, invoice_details_item_id, " +
                "invoice_details_item_name, invoice_details_item_price, invoice_details_date, invoice_details_item_qty," +
                " invoice_details_item_discount ,invoice_item_discount FROM invoice_details  " +
                "JOIN invoice ON invoice_details_invoice_id=invoice_id " +
                "WHERE invoice_details_invoice_id=?";
        Connection con = null;
        try {
            con = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement ps = con.prepareStatement(query);
        ps.setObject(1,invoiceId);
        ResultSet rst = ps.executeQuery();
        return rst;
    }
}
