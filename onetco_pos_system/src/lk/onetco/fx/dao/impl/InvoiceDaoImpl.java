package lk.onetco.fx.dao.impl;

import lk.onetco.fx.dao.InvoiceDao;
import lk.onetco.fx.database.DBConnection;
import lk.onetco.fx.model.Invoice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class InvoiceDaoImpl implements InvoiceDao {
    @Override
    public boolean addInvoice(Invoice invoice,boolean isReArrangedOrder,int reArrangedInvoiceID) throws SQLException {
        Connection connection = null;
        try {
            connection= DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement pstm = connection.prepareStatement("INSERT INTO invoice VALUES(?,?,?,?,?,?,?)");
        pstm.setObject(1,invoice.getInvoiceId());
        pstm.setObject(2,generateInvoiceCode());
        if(isReArrangedOrder){
            String invoiceCode="INV_"+reArrangedInvoiceID+"_UP";
            pstm.setObject(2,invoiceCode);
        }
        pstm.setObject(3,invoice.getInvoiceItemId());
        pstm.setObject(4,invoice.getInvoiceItemQty());
        pstm.setObject(5,invoice.getInvoiceItemPrice());
        pstm.setObject(6,invoice.getInvoicetemDiscount());
        pstm.setObject(7,invoice.getInvoiceTotalPrice());
        return pstm.executeUpdate()>0;
    }

    @Override
    public int getNextInvoiceId() throws SQLException {
        Connection connection = null;
        int maxInvoiceId=0;
        try {
            connection= DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement ps = connection.prepareStatement("select max(invoice_id) from invoice");
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {

                maxInvoiceId= rset.getInt(1);

        }
        return ++maxInvoiceId;
    }

    @Override
    public ResultSet getAllInvoices() throws SQLException {
        String query="SELECT DISTINCT invoice_id, invoice_code, invoice_item_id, invoice__item_qty, invoice_item_price, " +
                "invoice_item_discount, invoice_total_price, invoice_details_date FROM invoice " +
                "JOIN invoice_details ON invoice_id=invoice_details_invoice_id group by invoice_id";
        Connection con = null;
        try {
            con = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement ps = con.prepareStatement(query);
        ResultSet rst = ps.executeQuery();
        return rst;
    }


    public String generateInvoiceCode() throws SQLException{
        Connection connection = null;
        String invoiceCode="INV_";
        int maxInvoiceId=0;
        int nextId;
        try {
            connection= DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreparedStatement ps = connection.prepareStatement("select max(invoice_id) from invoice");
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
                maxInvoiceId= rset.getInt(1);
        }
        nextId=++maxInvoiceId;
        invoiceCode=invoiceCode+nextId;
        return invoiceCode;
    }
}
