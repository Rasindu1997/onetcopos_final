package lk.onetco.fx.dao;

import lk.onetco.fx.model.Payment;

import java.sql.SQLException;

public interface PaymentDao {
    boolean addPayment(Payment payment) throws SQLException;
}
