package lk.onetco.fx.dao;

import lk.onetco.fx.model.Customer;

import java.sql.SQLException;

public interface CustomerDao {
    boolean addCustomer(Customer customer) throws SQLException;
    int getNextCustomerId() throws SQLException;
}
