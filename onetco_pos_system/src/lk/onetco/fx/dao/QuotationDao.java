package lk.onetco.fx.dao;

import lk.onetco.fx.model.Quotation;

import java.sql.SQLException;

public interface QuotationDao {

    boolean addQuotaion(Quotation quotation) throws SQLException;
    int getNextQuotationId() throws SQLException;
}
