package lk.onetco.fx.dao;

import lk.onetco.fx.model.Item;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

public interface CommonDao {
    int getInvoiceCount() throws SQLException;
    int getItemCount() throws SQLException;
    int getCustomersCount() throws SQLException;
    BigDecimal getIncome() throws SQLException;
    ResultSet getTodayInvoices() throws SQLException;
}
