package lk.onetco.fx.dao;

import lk.onetco.fx.model.Item;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface ItemDao {

    boolean addItem(Item category) throws SQLException;

    boolean deleteItem(int id) throws SQLException;

    boolean updateItem(Item category) throws SQLException;

    ResultSet getAllItems() throws SQLException;

    ResultSet findItems(String itemName) throws SQLException;

    boolean reduceItemQty(int itemId,int qty) throws SQLException;
    boolean increaseItemQty(int itemId,int qty) throws SQLException;


}
