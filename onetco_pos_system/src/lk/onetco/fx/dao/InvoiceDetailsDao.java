package lk.onetco.fx.dao;

import lk.onetco.fx.model.InvoiceDetails;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface InvoiceDetailsDao {
    boolean addInvoiceDetails(InvoiceDetails invoiceDetails) throws SQLException;
    ResultSet getInvoiceDetailsByInvoiceId(int invoiceId) throws SQLException;
}
