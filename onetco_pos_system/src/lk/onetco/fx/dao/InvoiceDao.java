package lk.onetco.fx.dao;

import lk.onetco.fx.model.Invoice;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface InvoiceDao {

    boolean addInvoice(Invoice invoice,boolean isReArrangedOrder,int reArrangeInvoiceId) throws SQLException;
    int getNextInvoiceId() throws SQLException;
    ResultSet getAllInvoices() throws SQLException;

}
