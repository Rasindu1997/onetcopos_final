package lk.onetco.fx.dao;

import lk.onetco.fx.model.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface UserDao {

    boolean addUser(User user) throws SQLException;

    boolean deleteUser(int userid) throws SQLException;

    boolean updateUser(User user) throws SQLException;
    boolean updateUserPassword(String password,int userId) throws SQLException;

    boolean userLogin(String username,String password) throws  SQLException;

    ResultSet getAllUsers() throws SQLException;

    ResultSet getUserDetailsByUserName(String userName) throws SQLException;
}
