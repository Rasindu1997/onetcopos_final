package lk.onetco.fx.dao;

import lk.onetco.fx.model.Category;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface CategoryDao {

    boolean addCategory(Category category) throws SQLException;

    boolean deleteCategory(int id) throws SQLException;

    boolean updateCategory(Category category) throws SQLException;

    ResultSet getAllCategory() throws SQLException;

    String getCategoryNameByCategoryCode(String categorycode) throws SQLException;

}
