package lk.onetco.fx.view.controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lk.onetco.fx.dao.impl.InvoiceDaoImpl;
import lk.onetco.fx.dao.impl.InvoiceDetailsDaoImpl;
import lk.onetco.fx.dao.impl.ItemDaoImpl;
import lk.onetco.fx.model.Cart;
import lk.onetco.fx.model.Invoice;
import lk.onetco.fx.model.Item;
import lk.onetco.fx.validation.Validation;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Bidi;
import java.util.Date;
import java.util.Optional;
import java.util.ResourceBundle;

public class ReArrangeOrderController implements Initializable {

    @FXML
    private TableView<Item> itemTable;

    @FXML
    private TableColumn<Item, Integer> col_id;

    @FXML
    private TableColumn<Item, String> col_code;

    @FXML
    private TableColumn<Item, String> col_category;

    @FXML
    private TableColumn<Item, String> clol_name;

    @FXML
    private TableColumn<Item, String> col_qty;

    @FXML
    private Label txtItemName;

    @FXML
    private Label txtInvoiceTotal;

    @FXML
    private Label txtItemPrice;

    @FXML
    private JFXTextField fldQty;

    @FXML
    private Button btnAddToCart;

    @FXML
    private JFXComboBox<Integer> cmbDiscount;

    @FXML
    private TableView<Cart> cartTable;

    @FXML
    private ImageView imgClose;

    @FXML
    private TableColumn<Cart,Integer> col_id1;

    @FXML
    private TableColumn<Cart, String> col_ItemName2;

    @FXML
    private TableColumn<Cart, Integer> col_qty2;

    @FXML
    private TableColumn<Cart, BigDecimal> col_price2;

    @FXML
    private TableColumn<Cart, BigDecimal> col_discount;

    @FXML
    private TableColumn<Cart, BigDecimal> colTotal;

    @FXML
    private Button btnRemoveFromCart;

    @FXML
    private Button btnInvoice;

    @FXML
    private Button btnUpdateCart;

    @FXML
    private TableView<Invoice> invoiceTable;

    @FXML
    private TableColumn<Invoice,Integer> invoiceId;

    @FXML
    private TableColumn<Invoice,String> invoiceCode;

    @FXML
    private TableColumn<Invoice, java.util.Date> invoiceDate;

    final ObservableList<Item> items = FXCollections.observableArrayList();
    final ObservableList<Cart> cart = FXCollections.observableArrayList();
    final ObservableList<Invoice> Invoices = FXCollections.observableArrayList();
    final ObservableList<Integer> discountLevels = FXCollections.observableArrayList();
    Cart selectedTabelModel = null;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            setTableValues();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        itemTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Item>() {
            @Override
            public void changed(ObservableValue<? extends Item> observable, Item oldValue, Item selectedItem) {

                if (selectedItem == null) {
                    // Clear Selection
                    return;
                }
                txtItemName.setText(selectedItem.getItemName());
                txtItemPrice.setText(String.valueOf(selectedItem.getRetailPrice()));
            }
        });
        discountLevels.add(5);
        discountLevels.add(10);
        discountLevels.add(20);
        cmbDiscount.setItems(discountLevels);

        cartTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Cart>() {
            @Override
            public void changed(ObservableValue<? extends Cart> observable, Cart oldValue, Cart selectedItem) {

                if (selectedItem == null) {
                    // Clear Selection
                    return;
                }
                Cart cart = new Cart();
                txtItemName.setText(selectedItem.getItemName());
                txtItemPrice.setText(String.valueOf(selectedItem.getRetailPrice()));
                fldQty.setText(String.valueOf(selectedItem.getQty()));
                cart.setDiscount(selectedItem.getDiscount());
                selectedTabelModel = cartTable.getSelectionModel().getSelectedItem();
                int i = 0;
                while (i <= discountLevels.size()) {
                    if (discountLevels.get(i) == selectedItem.getDiscount().intValue()) {
                        break;
                    }
                    i++;
                }
                cmbDiscount.getSelectionModel().select(i);
            }
        });
        invoiceTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Invoice>() {
            @Override
            public void changed(ObservableValue<? extends Invoice> observable, Invoice oldValue, Invoice selectedItem) {

                if (selectedItem == null) {
                    // Clear Selection
                    return;
                }
                cartTable.getItems().clear();
                InvoiceDetailsDaoImpl invoiceDetailsDao =new InvoiceDetailsDaoImpl();
                ResultSet resultSet=null;
                PlaceOrder_Controller.invoiceId=invoiceTable.getSelectionModel().getSelectedItem().getInvoiceId();
                try {
                   resultSet = invoiceDetailsDao.getInvoiceDetailsByInvoiceId(selectedItem.getInvoiceId());
                    BigDecimal discountedTotal=BigDecimal.ZERO;
                    BigDecimal discountedLevel=BigDecimal.ZERO;
                    resultSet.beforeFirst();
                    while (resultSet.next()){
                        discountedLevel=resultSet.getBigDecimal("invoice_details_item_discount");
                        BigDecimal total = resultSet.getBigDecimal("invoice_details_item_price").multiply(BigDecimal.valueOf(resultSet.getInt("invoice_details_item_qty")));
                        BigDecimal discount = total.multiply(discountedLevel).divide(BigDecimal.valueOf(100));
                        discountedTotal= total.subtract(discount);
                        cart.add(new Cart(resultSet.getInt("invoice_details_item_id"),
                                resultSet.getString("invoice_details_item_name"),
                                resultSet.getBigDecimal("invoice_details_item_price"),
                                resultSet.getInt("invoice_details_item_qty"),
                                resultSet.getBigDecimal("invoice_details_item_discount"),
                                discountedTotal));

                    }
                    col_id1.setCellValueFactory(new PropertyValueFactory<Cart, Integer>("itemId"));
                    col_ItemName2.setCellValueFactory(new PropertyValueFactory<Cart, String>("itemName"));
                    col_qty2.setCellValueFactory(new PropertyValueFactory<Cart, Integer>("Qty"));
                    col_price2.setCellValueFactory(new PropertyValueFactory<Cart, BigDecimal>("retailPrice"));
                    col_discount.setCellValueFactory(new PropertyValueFactory<Cart, BigDecimal>("discount"));
                    colTotal.setCellValueFactory(new PropertyValueFactory<Cart, BigDecimal>("total"));
                    cartTable.setItems(cart);
                    calculateTotalInvoiceBalance();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    @FXML
    void btnAddToCartMouseClicked(MouseEvent event) {
        Alert confirmMsg = null;
        if (!(Validation.isNotEmpty(txtItemName.getText()))) {
            confirmMsg = new Alert(Alert.AlertType.WARNING, "Item Name is Required!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            return;
        }
        if (!(Validation.isNotEmpty(txtItemPrice.getText()))) {
            confirmMsg = new Alert(Alert.AlertType.WARNING, "Item Price is Required!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            return;
        }
        if (!Validation.isBigDecimalValue(txtItemPrice.getText())) {
            confirmMsg = new Alert(Alert.AlertType.WARNING, "Not a valid Item Price!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            return;
        }
        if (!Validation.isNotEmpty(fldQty.getText())) {
            confirmMsg = new Alert(Alert.AlertType.WARNING, "Item Qty is Required!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            return;
        }

        if (!Validation.isIntegerValue(fldQty.getText())) {
            confirmMsg = new Alert(Alert.AlertType.WARNING, "Please Enter valid Item Qty!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            return;
        }

        if (cmbDiscount.getSelectionModel().getSelectedIndex() == -1) {
            confirmMsg = new Alert(Alert.AlertType.WARNING, "Please Select a Discount Level!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            return;
        }
        BigDecimal total=BigDecimal.ZERO;
        if(itemTable.getSelectionModel()==null){
            total=cartTable.getSelectionModel().getSelectedItem().getRetailPrice();
        }else{
            total=itemTable.getSelectionModel().getSelectedItem().getRetailPrice();
        }
         total = itemTable.getSelectionModel().getSelectedItem().getRetailPrice().multiply(BigDecimal.valueOf(Long.parseLong(fldQty.getText())));
        BigDecimal discount = total.multiply(BigDecimal.valueOf(cmbDiscount.getSelectionModel().getSelectedItem())).divide(BigDecimal.valueOf(100));
        BigDecimal discountedTotal = total.subtract(discount);
        cart.add(new Cart(itemTable.getSelectionModel().getSelectedItem().getItemId(), txtItemName.getText(), itemTable.getSelectionModel().getSelectedItem().getRetailPrice(), Integer.valueOf(fldQty.getText()), BigDecimal.valueOf(cmbDiscount.getSelectionModel().getSelectedItem()), discountedTotal));
        col_id1.setCellValueFactory(new PropertyValueFactory<Cart, Integer>("itemId"));
        col_ItemName2.setCellValueFactory(new PropertyValueFactory<Cart, String>("itemName"));
        col_qty2.setCellValueFactory(new PropertyValueFactory<Cart, Integer>("Qty"));
        col_price2.setCellValueFactory(new PropertyValueFactory<Cart, BigDecimal>("retailPrice"));
        col_discount.setCellValueFactory(new PropertyValueFactory<Cart, BigDecimal>("discount"));
        colTotal.setCellValueFactory(new PropertyValueFactory<Cart, BigDecimal>("total"));
        cartTable.setItems(cart);
        calculateTotalInvoiceBalance();
        resetFileds();
    }

    @FXML
    void btnInvoiceMouseClicked(MouseEvent event) {
        if(cartTable.getItems().size()==0){
            new Alert(Alert.AlertType.WARNING, "Please add items to Proceed!", ButtonType.OK).showAndWait();
            return;
        }
        PlaceOrder_Controller.cartItems = cart;
        PlaceOrder_Controller.isReArrangedOrder=true;
        PlaceOrder_Controller.invoicTotalPrice = new BigDecimal(txtInvoiceTotal.getText());

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/lk/onetco/fx/view/PlaceOrder.fxml"));
        Parent root = null;
        try {
            root = (Parent) fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.initStyle(StageStyle.UNDECORATED);
        stage.show();
    }

    @FXML
    void btnRemoveMouseClicked(MouseEvent event) {
        ItemDaoImpl itemDao =new ItemDaoImpl();
        if (cartTable.getSelectionModel().getSelectedItem() == null) {
            Alert confirmMsg = new Alert(Alert.AlertType.WARNING, "Please Select Item to Delete!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
        } else {
            Alert confirmMsg = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure to Delete this item from this Invoice?", ButtonType.YES, ButtonType.NO);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            if (buttonType.get() == ButtonType.YES) {
                Cart selectedItem = cartTable.getSelectionModel().getSelectedItem();
                try {
                    itemDao.increaseItemQty(cartTable.getSelectionModel().getSelectedItem().getItemId(),
                            cartTable.getSelectionModel().getSelectedItem().getQty());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                cartTable.getItems().remove(selectedItem);
                calculateTotalInvoiceBalance();
                resetFileds();
            }
        }
    }

    @FXML
    void btnUpdateCartMouseClicked(MouseEvent event) {
        if (cartTable.getSelectionModel().getSelectedItem() == null) {
            Alert confirmMsg = new Alert(Alert.AlertType.WARNING, "Please Select Item to Update!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
        } else {
            Alert confirmMsg = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure to Update this item?", ButtonType.YES, ButtonType.NO);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            if (buttonType.get() == ButtonType.YES) {
                if (!(Validation.isNotEmpty(txtItemName.getText()))) {
                    confirmMsg = new Alert(Alert.AlertType.WARNING, "Item Name is Required!", ButtonType.OK);
                    Optional<ButtonType> buttonType1 = confirmMsg.showAndWait();
                    return;
                }
                if (!(Validation.isNotEmpty(txtItemPrice.getText()))) {
                    confirmMsg = new Alert(Alert.AlertType.WARNING, "Item Price is Required!", ButtonType.OK);
                    Optional<ButtonType> buttonType1 = confirmMsg.showAndWait();
                    return;
                }
                if (!Validation.isBigDecimalValue(txtItemPrice.getText())) {
                    confirmMsg = new Alert(Alert.AlertType.WARNING, "Not a valid Item Price!", ButtonType.OK);
                    Optional<ButtonType> buttonType1 = confirmMsg.showAndWait();
                    return;
                }
                if (!Validation.isNotEmpty(fldQty.getText())) {
                    confirmMsg = new Alert(Alert.AlertType.WARNING, "Item Qty is Required!", ButtonType.OK);
                    Optional<ButtonType> buttonType1 = confirmMsg.showAndWait();
                    return;
                }

                if (!Validation.isIntegerValue(fldQty.getText())) {
                    confirmMsg = new Alert(Alert.AlertType.WARNING, "Please Enter valid Item Qty!", ButtonType.OK);
                    Optional<ButtonType> buttonType1 = confirmMsg.showAndWait();
                    return;
                }

                if (cmbDiscount.getSelectionModel().getSelectedIndex() == -1) {
                    confirmMsg = new Alert(Alert.AlertType.WARNING, "Please Select a Discount Level!", ButtonType.OK);
                    Optional<ButtonType> buttonType1 = confirmMsg.showAndWait();
                    return;
                }

                BigDecimal total = cartTable.getSelectionModel().getSelectedItem().getRetailPrice().multiply(Validation.getBigDecimalValueFromString(fldQty.getText()));
                BigDecimal discount = total.multiply(BigDecimal.valueOf(cmbDiscount.getSelectionModel().getSelectedItem())).divide(BigDecimal.valueOf(100));
                BigDecimal discountedTotal = total.subtract(discount);

                cart.add(new Cart(cartTable.getSelectionModel().getSelectedItem().getItemId(), txtItemName.getText(), Validation.getBigDecimalValueFromString(txtItemPrice.getText()), Validation.getIntValueFromString(fldQty.getText()), BigDecimal.valueOf(cmbDiscount.getSelectionModel().getSelectedItem()), discountedTotal));
                col_id1.setCellValueFactory(new PropertyValueFactory<Cart, Integer>("itemId"));
                col_ItemName2.setCellValueFactory(new PropertyValueFactory<Cart, String>("itemName"));
                col_qty2.setCellValueFactory(new PropertyValueFactory<Cart, Integer>("Qty"));
                col_price2.setCellValueFactory(new PropertyValueFactory<Cart, BigDecimal>("retailPrice"));
                col_discount.setCellValueFactory(new PropertyValueFactory<Cart, BigDecimal>("discount"));
                colTotal.setCellValueFactory(new PropertyValueFactory<Cart, BigDecimal>("total"));
                cartTable.setItems(cart);
                calculateTotalInvoiceBalance();
                resetFileds();
            }
        }
    }

    public void calculateTotalInvoiceBalance() {
        int i = 0;
        BigDecimal invoiceTotal = BigDecimal.ZERO;
        while (i < cartTable.getItems().size()) {
            invoiceTotal = invoiceTotal.add(cartTable.getItems().get(i).getTotal());
            i++;
        }
        txtInvoiceTotal.setText(String.valueOf(invoiceTotal));
    }

    public void resetFileds() {
        txtItemName.setText(null);
        txtItemPrice.setText(null);
        fldQty.setText(null);
        cmbDiscount.getSelectionModel().select(-1);
    }

    public void setTableValues() throws SQLException {
        itemTable.getItems().clear();
        ItemDaoImpl item = new ItemDaoImpl();
        ResultSet rst = item.getAllItems();
        while (rst.next()) {
            items.add(new Item(rst.getInt("item_id"), rst.getString("item_code"),
                    rst.getString("item_category_id"), rst.getString("item_name"), rst.getBigDecimal("item_retail_price"),
                    rst.getInt("item_stock_qty"),rst.getString("item_warranty")));
        }
        col_id.setCellValueFactory(new PropertyValueFactory<Item, Integer>("itemId"));
        col_code.setCellValueFactory(new PropertyValueFactory<Item, String>("itemCode"));
        col_category.setCellValueFactory(new PropertyValueFactory<Item, String>("itemCategoryCode"));
        clol_name.setCellValueFactory(new PropertyValueFactory<Item, String>("itemName"));
        col_qty.setCellValueFactory(new PropertyValueFactory<Item, String>("itemStockQty"));
        itemTable.setItems(items);

        invoiceTable.getItems().clear();
        InvoiceDaoImpl invoiceDao =new InvoiceDaoImpl();
        ResultSet rst2=invoiceDao.getAllInvoices();
        while (rst2.next()){
            Invoices.add(new Invoice(rst2.getInt("invoice_id"),rst2.getString("invoice_code"),
                    rst2.getDate("invoice_details_date")));
        }
        invoiceId.setCellValueFactory(new PropertyValueFactory<Invoice, Integer>("invoiceId"));
        invoiceCode.setCellValueFactory(new PropertyValueFactory<Invoice, String>("invoiceCode"));
        invoiceDate.setCellValueFactory(new PropertyValueFactory<Invoice, Date>("invoiceDate"));
        invoiceTable.setItems(Invoices);
    }

    @FXML
    void imgCloseMouseClicked(MouseEvent event) {
        Stage stage = (Stage) imgClose.getScene().getWindow();
        stage.close();
    }

}
