package lk.onetco.fx.view.controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.animation.ScaleTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.*;
import javafx.util.Duration;
import lk.onetco.fx.dao.impl.ItemDaoImpl;
import lk.onetco.fx.dao.impl.QuotationDaoImpl;
import lk.onetco.fx.database.DBConnection;
import lk.onetco.fx.model.Cart;
import lk.onetco.fx.model.Item;
import lk.onetco.fx.model.Quotation;
import lk.onetco.fx.validation.Validation;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

public class UserDashboard_Controller implements Initializable {


    private double xOffset = 0;
    private double yOffset = 0;
    @FXML
    private Label txtItemPrice;

    @FXML
    private JFXTextField fldQty;

    @FXML
    private JFXTextField fldDiscount;

    @FXML
    private Button btnAddToCart;

    @FXML
    private JFXComboBox<Integer> cmbDiscount;

    @FXML
    private Label txtItemName;

    @FXML
    private TableView<Cart> cartTable;

    @FXML
    private TableColumn<Cart, Integer> col_id1;

    @FXML
    private TableColumn<Cart, String> col_ItemName2;

    @FXML
    private TableColumn<Cart, Integer> col_qty2;

    @FXML
    private TableColumn<Cart, BigDecimal> col_price2;

    @FXML
    private TableColumn<Cart, BigDecimal> col_discount;

    @FXML
    private TableColumn<Cart, BigDecimal> colTotal;

    @FXML
    private Button btnUpdateCart;

    @FXML
    private TextField searchText;

    @FXML
    private Label txtInvoiceTotal;

    @FXML
    private Button btnRemoveFromCart;

    @FXML
    private ImageView imgInvoice;

    @FXML
    private ImageView imgReports;

    @FXML
    private ImageView imgSignOut;

    @FXML
    private ImageView imgStockManagement;

    @FXML
    private ImageView imgUserManagement;

    @FXML
    private ImageView imgUser;

    @FXML
    private ImageView categoryManagement;

    @FXML
    private TableView<Item> itemTable;

    @FXML
    private TableColumn<Item, Integer> col_id;

    @FXML
    private TableColumn<Item, String> col_code;

    @FXML
    private TableColumn<Item, String> col_category;

    @FXML
    private TableColumn<Item, String> clol_name;

    @FXML
    private TableColumn<Item, BigDecimal> col_price;

    @FXML
    private TableColumn<Item, String> col_qty;

    @FXML
    private Button btnInvoice;

    @FXML
    private Button quto_id;

    @FXML
    private ImageView imgClose;

    @FXML
    private ImageView imgMinimize;

    @FXML
    private VBox vbMenu;

    FileChooser fileChooser = new FileChooser();

    Cart selectedTabelModel = null;

    final ObservableList<Item> items = FXCollections.observableArrayList();
    final ObservableList<Cart> cart = FXCollections.observableArrayList();
    final ObservableList<Integer> discountLevels = FXCollections.observableArrayList();

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {
            setTableValues();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        itemTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Item>() {
            @Override
            public void changed(ObservableValue<? extends Item> observable, Item oldValue, Item selectedItem) {

                if (selectedItem == null) {
                    // Clear Selection
                    return;
                }
                txtItemName.setText(selectedItem.getItemName());
                txtItemPrice.setText(String.valueOf(selectedItem.getRetailPrice()));
            }
        });
        discountLevels.add(5);
        discountLevels.add(10);
        discountLevels.add(20);
        cmbDiscount.setItems(discountLevels);
        cartTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Cart>() {
            @Override
            public void changed(ObservableValue<? extends Cart> observable, Cart oldValue, Cart selectedItem) {

                if (selectedItem == null) {
                    // Clear Selection
                    return;
                }
                Cart cart = new Cart();
                txtItemName.setText(selectedItem.getItemName());
                txtItemPrice.setText(String.valueOf(selectedItem.getRetailPrice()));
                fldQty.setText(String.valueOf(selectedItem.getQty()));
                cart.setDiscount(selectedItem.getDiscount());
                selectedTabelModel = cartTable.getSelectionModel().getSelectedItem();
                int i = 0;
                while (i <= discountLevels.size()) {
                    if (discountLevels.get(i) == selectedItem.getDiscount().intValue()) {
                        break;
                    }
                    i++;
                }
                cmbDiscount.getSelectionModel().select(i);
            }
        });
    }

    @FXML
    void imgCloseMouseClicked(MouseEvent event) {
        Stage stage = (Stage) imgClose.getScene().getWindow();
        stage.close();
    }

    @FXML
    void searchTextReleased(KeyEvent event) throws SQLException {

        itemTable.getItems().clear();
        ItemDaoImpl itemDao = new ItemDaoImpl();
        ResultSet rst = null;
        try {
            rst = itemDao.findItems(searchText.getText());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        while (rst.next()) {
            items.add(new Item(rst.getInt("item_id"), rst.getString("item_code"),
                    rst.getString("item_category_id"), rst.getString("item_name"), rst.getBigDecimal("item_retail_price"),
                    rst.getInt("item_stock_qty"),rst.getString("item_warranty")));
        }
        col_id.setCellValueFactory(new PropertyValueFactory<Item, Integer>("itemId"));
        col_code.setCellValueFactory(new PropertyValueFactory<Item, String>("itemCode"));
        col_category.setCellValueFactory(new PropertyValueFactory<Item, String>("itemCategoryCode"));
        clol_name.setCellValueFactory(new PropertyValueFactory<Item, String>("itemName"));
        col_price.setCellValueFactory(new PropertyValueFactory<Item, BigDecimal>("retailPrice"));
        col_qty.setCellValueFactory(new PropertyValueFactory<Item, String>("itemStockQty"));
        itemTable.setItems(items);

    }


    @FXML
    void btnInvoiceMouseClicked(MouseEvent event) throws IOException {
//        Alert alert = new Alert(Alert.AlertType.INFORMATION);
//        alert.setContentText("You want to continue?");
//        alert.show();
        if(cartTable.getItems().size()==0){
            new Alert(Alert.AlertType.WARNING, "Please add items to Proceed!", ButtonType.OK).showAndWait();
            return;
        }
        PlaceOrder_Controller.cartItems = cart;
        PlaceOrder_Controller.invoicTotalPrice = new BigDecimal(txtInvoiceTotal.getText());

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/lk/onetco/fx/view/PlaceOrder.fxml"));
        Parent root = (Parent) fxmlLoader.load();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.initStyle(StageStyle.UNDECORATED);
        stage.show();
        stage.setOnHidden(event1 -> {
            try {
                event1.consume();
                setTableValues();
                resetFileds();
                txtInvoiceTotal.setText(null);
                cartTable.getItems().clear();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    @FXML
    void addUser(MouseEvent event) {

    }

    @FXML
    void btnStockManagementMouseClicked(MouseEvent event) {

    }

    @FXML
    void categoryManagementClciked(MouseEvent event) {

    }


    public  void setTableValues() throws SQLException {
        itemTable.getItems().clear();
        ItemDaoImpl item = new ItemDaoImpl();
        ResultSet rst = item.getAllItems();
        while (rst.next()) {
            items.add(new Item(rst.getInt("item_id"), rst.getString("item_code"),
                    rst.getString("item_category_id"), rst.getString("item_name"), rst.getBigDecimal("item_retail_price"),
                    rst.getInt("item_stock_qty"),rst.getString("item_warranty")));
        }
        col_id.setCellValueFactory(new PropertyValueFactory<Item, Integer>("itemId"));
        col_code.setCellValueFactory(new PropertyValueFactory<Item, String>("itemCode"));
        col_category.setCellValueFactory(new PropertyValueFactory<Item, String>("itemCategoryCode"));
        clol_name.setCellValueFactory(new PropertyValueFactory<Item, String>("itemName"));
        col_price.setCellValueFactory(new PropertyValueFactory<Item, BigDecimal>("retailPrice"));
        col_qty.setCellValueFactory(new PropertyValueFactory<Item, String>("itemStockQty"));
        itemTable.setItems(items);
    }


    @FXML
    void playMouseEnterAnimation(MouseEvent event) {
        if (event.getSource() instanceof ImageView) {
            ImageView icon = (ImageView) event.getSource();

            switch (icon.getId()) {
                case "imgStockManagement":
//                    lblMenu.setText("Stock Management");
                    break;
                case "imgUserManagement":
//                    lblMenu.setText("User Management");
                    break;
                case "imgInvoice":
//                    lblMenu.setText("InvoiceDao Details");
                    break;
                case "imgUser":
//                    lblMenu.setText("User Details");
                    break;
                case "imgReports":
//                    lblMenu.setText("Reports");
                    break;
                case "categoryManagement":
//                    lblMenu.setText("Manage Categories");
                    break;
            }

            ScaleTransition scaleT = new ScaleTransition(Duration.millis(200), icon);
            scaleT.setToX(1.2);
            scaleT.setToY(1.2);
            scaleT.play();

            DropShadow glow = new DropShadow();
            glow.setColor(Color.SKYBLUE);
            glow.setWidth(20);
            glow.setHeight(20);
            glow.setRadius(20);
            icon.setEffect(glow);
        }
    }

    @FXML
    void btnQuoteMouseClicked(MouseEvent event) {
            if(cartTable.getItems().size()==0){
                new Alert(Alert.AlertType.WARNING, "Please add items to Print the Quotation!", ButtonType.OK).showAndWait();
                return;
            }
        try {
            QuotationDaoImpl quotationDao = new QuotationDaoImpl();
            Quotation quotation = new Quotation();
            String quotationCode = QuotationDaoImpl.generateQuotationCode();
            int i = 0;
            while (i < cart.size()) {
                quotation.setQuotationCode(quotationCode);
                quotation.setItemName(cart.get(i).getItemName());
                quotation.setItemQuantity(cart.get(i).getQty());
                quotation.setItemPrice(cart.get(i).getRetailPrice());
                quotation.setQuotationDate(new Date());
                quotationDao.addQuotaion(quotation);
                i++;
            }


            Map<String, Object> map = new HashMap<>();
            int k = 0;
            BigDecimal invoiceTotal = BigDecimal.ZERO;
            while (k < cartTable.getItems().size()) {
                invoiceTotal = invoiceTotal.add(cartTable.getItems().get(k).getTotal());
                k++;
            }
            map.put("quotationCode", quotationCode);
            map.put("quotationPrice", invoiceTotal);
            JasperDesign jasperDesign = JRXmlLoader.load(this.getClass().getResourceAsStream("/lk/onetco/fx/reports/quotation.jrxml"));
            Connection con = DBConnection.getConnection();
            JasperReport compileReport = JasperCompileManager.compileReport(jasperDesign);
            JasperPrint jasperPrint = JasperFillManager.fillReport(compileReport, map, con);
//       JasperViewer.viewReport(jasperPrint);
            String fileDir = saveFileDirectoryPicker();
            if(!(Validation.isNotEmpty(fileDir))){
                new Alert(Alert.AlertType.WARNING, "Please Select a folder to save Report!", ButtonType.OK).showAndWait();
                return;
            }
            JasperExportManager.exportReportToPdfFile(jasperPrint, fileDir + "\\Quotation_" + new SimpleDateFormat("yyyy-MM-dd-HH-mm").format(new Date()) + ".pdf");
        } catch (JRException e1) {
            e1.printStackTrace();
        } catch (Exception e1) {
            e1.printStackTrace();
        }

    }


    public String saveFileDirectoryPicker() {
        Window stage = vbMenu.getScene().getWindow(); //get a handle to the stage
        fileChooser.setTitle("Save File"); //set the title of the Dialog window
        String defaultSaveName = "Quotation_" + new SimpleDateFormat("yyyy-MM-dd-HH-mm'.txt'").format(new Date());
        fileChooser.setInitialFileName(defaultSaveName);
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PDF Files", "*.pdf"),
                new FileChooser.ExtensionFilter("Text Files", "*.txt"));
        try {
            File file = fileChooser.showSaveDialog(stage);
            if (file != null) {
                File dir = file.getParentFile();//gets the selected directory
                fileChooser.setInitialDirectory(dir);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if(fileChooser.getInitialDirectory()!=null){
            return fileChooser.getInitialDirectory().getAbsolutePath();
        }
        else{
            return "";
        }
    }

    @FXML
    void btnAddToCartMouseClicked(MouseEvent event) {
        Alert confirmMsg = null;
        if (!(Validation.isNotEmpty(txtItemName.getText()))) {
            confirmMsg = new Alert(Alert.AlertType.WARNING, "Item Name is Required!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            return;
        }
        if (!(Validation.isNotEmpty(txtItemPrice.getText()))) {
            confirmMsg = new Alert(Alert.AlertType.WARNING, "Item Price is Required!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            return;
        }
        if (!Validation.isBigDecimalValue(txtItemPrice.getText())) {
            confirmMsg = new Alert(Alert.AlertType.WARNING, "Not a valid Item Price!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            return;
        }
        if (!Validation.isNotEmpty(fldQty.getText())) {
            confirmMsg = new Alert(Alert.AlertType.WARNING, "Item Qty is Required!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            return;
        }

        if (!Validation.isIntegerValue(fldQty.getText())) {
            confirmMsg = new Alert(Alert.AlertType.WARNING, "Please Enter valid Item Qty!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            return;
        }

        if (cmbDiscount.getSelectionModel().getSelectedIndex() == -1) {
            confirmMsg = new Alert(Alert.AlertType.WARNING, "Please Select a Discount Level!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            return;
        }

        BigDecimal total = itemTable.getSelectionModel().getSelectedItem().getRetailPrice().multiply(BigDecimal.valueOf(Long.parseLong(fldQty.getText())));
        BigDecimal discount = total.multiply(BigDecimal.valueOf(cmbDiscount.getSelectionModel().getSelectedItem())).divide(BigDecimal.valueOf(100));
        BigDecimal discountedTotal = total.subtract(discount);
        cart.add(new Cart(itemTable.getSelectionModel().getSelectedItem().getItemId(), txtItemName.getText(), itemTable.getSelectionModel().getSelectedItem().getRetailPrice(), Integer.valueOf(fldQty.getText()), BigDecimal.valueOf(cmbDiscount.getSelectionModel().getSelectedItem()), discountedTotal));
        col_id1.setCellValueFactory(new PropertyValueFactory<Cart, Integer>("itemId"));
        col_ItemName2.setCellValueFactory(new PropertyValueFactory<Cart, String>("itemName"));
        col_qty2.setCellValueFactory(new PropertyValueFactory<Cart, Integer>("Qty"));
        col_price2.setCellValueFactory(new PropertyValueFactory<Cart, BigDecimal>("retailPrice"));
        col_discount.setCellValueFactory(new PropertyValueFactory<Cart, BigDecimal>("discount"));
        colTotal.setCellValueFactory(new PropertyValueFactory<Cart, BigDecimal>("total"));
        cartTable.setItems(cart);
        calculateTotalInvoiceBalance();
        resetFileds();
    }

    @FXML
    void btnRemoveMouseClicked(MouseEvent event) {
        if (cartTable.getSelectionModel().getSelectedItem() == null) {
            Alert confirmMsg = new Alert(Alert.AlertType.WARNING, "Please Select Item to Delete!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
        } else {
            Cart selectedItem = cartTable.getSelectionModel().getSelectedItem();
            cartTable.getItems().remove(selectedItem);
            calculateTotalInvoiceBalance();
            resetFileds();
        }
    }

    @FXML
    void btnUpdateCartMouseClicked(MouseEvent event) {
//        cartTable.getItems().remove(selectedTabelModel);

        if (cartTable.getSelectionModel().getSelectedItem() == null) {
            Alert confirmMsg = new Alert(Alert.AlertType.WARNING, "Please Select Item to Update!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
        } else {
            Alert confirmMsg = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure to Update this item?", ButtonType.YES, ButtonType.NO);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            if (buttonType.get() == ButtonType.YES) {
                if (!(Validation.isNotEmpty(txtItemName.getText()))) {
                    confirmMsg = new Alert(Alert.AlertType.WARNING, "Item Name is Required!", ButtonType.OK);
                    Optional<ButtonType> buttonType1 = confirmMsg.showAndWait();
                    return;
                }
                if (!(Validation.isNotEmpty(txtItemPrice.getText()))) {
                    confirmMsg = new Alert(Alert.AlertType.WARNING, "Item Price is Required!", ButtonType.OK);
                    Optional<ButtonType> buttonType1 = confirmMsg.showAndWait();
                    return;
                }
                if (!Validation.isBigDecimalValue(txtItemPrice.getText())) {
                    confirmMsg = new Alert(Alert.AlertType.WARNING, "Not a valid Item Price!", ButtonType.OK);
                    Optional<ButtonType> buttonType1 = confirmMsg.showAndWait();
                    return;
                }
                if (!Validation.isNotEmpty(fldQty.getText())) {
                    confirmMsg = new Alert(Alert.AlertType.WARNING, "Item Qty is Required!", ButtonType.OK);
                    Optional<ButtonType> buttonType1 = confirmMsg.showAndWait();
                    return;
                }

                if (!Validation.isIntegerValue(fldQty.getText())) {
                    confirmMsg = new Alert(Alert.AlertType.WARNING, "Please Enter valid Item Qty!", ButtonType.OK);
                    Optional<ButtonType> buttonType1 = confirmMsg.showAndWait();
                    return;
                }

                if (cmbDiscount.getSelectionModel().getSelectedIndex() == -1) {
                    confirmMsg = new Alert(Alert.AlertType.WARNING, "Please Select a Discount Level!", ButtonType.OK);
                    Optional<ButtonType> buttonType1 = confirmMsg.showAndWait();
                    return;
                }
                BigDecimal total = cartTable.getSelectionModel().getSelectedItem().getRetailPrice().multiply(Validation.getBigDecimalValueFromString(fldQty.getText()));
                BigDecimal discount = total.multiply(BigDecimal.valueOf(cmbDiscount.getSelectionModel().getSelectedItem())).divide(BigDecimal.valueOf(100));
                BigDecimal discountedTotal = total.subtract(discount);

                cart.add(new Cart(itemTable.getSelectionModel().getSelectedItem().getItemId(), txtItemName.getText(), Validation.getBigDecimalValueFromString(txtItemPrice.getText()), Validation.getIntValueFromString(fldQty.getText()), BigDecimal.valueOf(cmbDiscount.getSelectionModel().getSelectedItem()), discountedTotal));
                col_id1.setCellValueFactory(new PropertyValueFactory<Cart, Integer>("itemId"));
                col_ItemName2.setCellValueFactory(new PropertyValueFactory<Cart, String>("itemName"));
                col_qty2.setCellValueFactory(new PropertyValueFactory<Cart, Integer>("Qty"));
                col_price2.setCellValueFactory(new PropertyValueFactory<Cart, BigDecimal>("retailPrice"));
                col_discount.setCellValueFactory(new PropertyValueFactory<Cart, BigDecimal>("discount"));
                colTotal.setCellValueFactory(new PropertyValueFactory<Cart, BigDecimal>("total"));
                cartTable.setItems(cart);
                calculateTotalInvoiceBalance();
                resetFileds();
            }
        }
    }

    public void calculateTotalInvoiceBalance() {
        int i = 0;
        BigDecimal invoiceTotal = BigDecimal.ZERO;
        while (i < cartTable.getItems().size()) {
            invoiceTotal = invoiceTotal.add(cartTable.getItems().get(i).getTotal());
            i++;
        }
        txtInvoiceTotal.setText(String.valueOf(invoiceTotal));
    }

    @FXML
    void playMouseExitAnimation(MouseEvent event) {
        if (event.getSource() instanceof ImageView) {
            ImageView icon = (ImageView) event.getSource();
            ScaleTransition scaleT = new ScaleTransition(Duration.millis(200), icon);
            scaleT.setToX(1);
            scaleT.setToY(1);
            scaleT.play();

            icon.setEffect(null);
//            lblMenu.setText("Admin Dashboard");
        }
    }

    @FXML
    void btnSignOut(MouseEvent event) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/lk/onetco/fx/view/Login.fxml"));
        Parent root = null;
        try {
            root = (Parent) fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.initStyle(StageStyle.UNDECORATED);
        stage.show();
        ((Node) (event.getSource())).getScene().getWindow().hide();
    }

    @FXML
    void invoiceDetailsMouseClicked(MouseEvent event) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/lk/onetco/fx/view/ReArrangeOrder.fxml"));
        Parent root = null;
        try {
            root = (Parent) fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.initStyle(StageStyle.UNDECORATED);
        stage.show();
    }

    public void resetFileds() {
        txtItemName.setText(null);
        txtItemPrice.setText(null);
        fldQty.setText(null);
        cmbDiscount.getSelectionModel().select(-1);
    }

}
