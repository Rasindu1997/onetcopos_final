package lk.onetco.fx.view.controller;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

public class StockManagement_Controller {

    private double xOffset = 0;
    private double yOffset = 0;

    @FXML
    private AnchorPane root;

    @FXML
    private Label btnAdditem;

    @FXML
    private Label btnViewItems;



    @FXML
    void btnAddItemMouseClicked(MouseEvent event) {
        try {
            FXMLLoader fxmlLoader =  new FXMLLoader(getClass().getResource("/lk/onetco/fx/view/AddItem.fxml"));
            Parent root=(Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.initStyle(StageStyle.UNDECORATED);
            stage.show();
            ((Node)(event.getSource())).getScene().getWindow().hide();

            root.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    xOffset = event.getSceneX();
                    yOffset = event.getSceneY();
                }
            });
            root.setOnMouseDragged(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    stage.setX(event.getScreenX() - xOffset);
                    stage.setY(event.getScreenY() - yOffset);
                }
            });
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void btnViewItemsOnMouseClicked(MouseEvent event) {
        try{
        FXMLLoader fxmlLoader =  new FXMLLoader(getClass().getResource("/lk/onetco/fx/view/ViewItem.fxml"));
        Parent root=(Parent) fxmlLoader.load();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.initStyle(StageStyle.UNDECORATED);
        stage.show();
        ((Node)(event.getSource())).getScene().getWindow().hide();

        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                stage.setX(event.getScreenX() - xOffset);
                stage.setY(event.getScreenY() - yOffset);
            }
        });
    }
        catch (IOException e) {
        e.printStackTrace();
    }
    }
}
