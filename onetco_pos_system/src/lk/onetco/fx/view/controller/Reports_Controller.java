package lk.onetco.fx.view.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import lk.onetco.fx.database.DBConnection;
import lk.onetco.fx.validation.Validation;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

import java.io.File;
import java.net.URL;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class Reports_Controller implements Initializable {
    FileChooser fileChooser = new FileChooser();
    @FXML
    private VBox vbMenu;

    @FXML
    private JFXDatePicker datePkrFromDate;

    @FXML
    private JFXDatePicker datePkrToDate;

    @FXML
    private JFXButton btnItemSales;

    @FXML
    private JFXButton btnCatSales;

    @FXML
    private JFXButton btnStockReport;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        datePkrToDate.setValue(LocalDate.now());
        datePkrFromDate.setValue(LocalDate.now());
    }

    @FXML
    void stockReportBtnClicked(MouseEvent event) throws JRException {
        LocalDate toDate=datePkrToDate.getValue();
        LocalDate fromDate=datePkrFromDate.getValue();
        System.out.println(toDate);
        System.out.println(fromDate);

        JasperDesign jasperDesign = JRXmlLoader.load(this.getClass().getResourceAsStream("/lk/onetco/fx/reports/Item_A4.jrxml"));
        Connection con=null;
        try {
            con = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        JasperReport compileReport = JasperCompileManager.compileReport(jasperDesign);
        JasperPrint jasperPrint = JasperFillManager.fillReport(compileReport, new HashMap<>(), con);
//        JasperViewer.viewReport(jasperPrint);
        String fileDir=saveFileDirectoryPicker();
        JasperExportManager.exportReportToPdfFile(jasperPrint, fileDir+"\\Item_A4"+new SimpleDateFormat("yyyy-MM-dd-HH-mm'.txt'").format(new Date())+".pdf");
    }

    @FXML
    void categoryItemSalesReportBtnClicked(MouseEvent event) throws JRException {
        LocalDate toDate=datePkrToDate.getValue();
        LocalDate fromDate=datePkrFromDate.getValue();
        java.util.Date todate = java.sql.Date.valueOf(toDate);
        java.util.Date fromdate = java.sql.Date.valueOf(fromDate);
        Map<String, Object> map = new HashMap<>();
        map.put("fromDate", fromdate);
        map.put("toDate", todate);
        JasperDesign jasperDesign = JRXmlLoader.load(this.getClass().getResourceAsStream("/lk/onetco/fx/reports/Category_Sales_A4.jrxml"));
        Connection con=null;
        try {
            con = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        JasperReport compileReport = JasperCompileManager.compileReport(jasperDesign);
        JasperPrint jasperPrint = JasperFillManager.fillReport(compileReport, map, con);
//        JasperViewer.viewReport(jasperPrint);
        String fileDir=saveFileDirectoryPicker();
        if(!(Validation.isNotEmpty(fileDir))){
            new Alert(Alert.AlertType.WARNING, "Please Select a folder to save Report!", ButtonType.OK).showAndWait();
            return;
        }
        JasperExportManager.exportReportToPdfFile(jasperPrint, fileDir+"\\CATEGORY_WISE_SALES_A4"+new SimpleDateFormat("yyyy-MM-dd-HH-mm'.txt'").format(new Date())+".pdf");

    }

    @FXML
    void itemSalesReportBtnClicked(MouseEvent event) throws JRException {
        LocalDate toDate=datePkrToDate.getValue();
        LocalDate fromDate=datePkrFromDate.getValue();
        java.util.Date todate = java.sql.Date.valueOf(toDate);
        java.util.Date fromdate = java.sql.Date.valueOf(fromDate);
        Map<String, Object> map = new HashMap<>();
        map.put("fromDate", fromdate);
        map.put("toDate", todate);
        JasperDesign jasperDesign = JRXmlLoader.load(this.getClass().getResourceAsStream("/lk/onetco/fx/reports/Item_Sales_A4.jrxml"));
        Connection con=null;
        try {
            con = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        JasperReport compileReport = JasperCompileManager.compileReport(jasperDesign);
        JasperPrint jasperPrint = JasperFillManager.fillReport(compileReport, map, con);
//        JasperViewer.viewReport(jasperPrint);
        String fileDir=saveFileDirectoryPicker();
        if(!(Validation.isNotEmpty(fileDir))){
            new Alert(Alert.AlertType.WARNING, "Please Select a folder to save Report!", ButtonType.OK).showAndWait();
            return;
        }
        JasperExportManager.exportReportToPdfFile(jasperPrint, fileDir+"\\ITEM_WISE_SALES_A4"+new SimpleDateFormat("yyyy-MM-dd-HH-mm'.txt'").format(new Date())+".pdf");

    }

    public String saveFileDirectoryPicker(){
        Window stage = vbMenu.getScene().getWindow(); //get a handle to the stage
        fileChooser.setTitle("Save File"); //set the title of the Dialog window
        String defaultSaveName = "Stock";
        fileChooser.setInitialFileName(defaultSaveName);
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("pdf Files", "*.pdf"),
                new FileChooser.ExtensionFilter("Text Files", "*.txt"));
        try
        {
            File file = fileChooser.showSaveDialog(stage);
            if (file != null)
            {
                File dir = file.getParentFile();//gets the selected directory
                fileChooser.setInitialDirectory(dir);
            }
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
        if(fileChooser.getInitialDirectory()!=null){
            return fileChooser.getInitialDirectory().getAbsolutePath();
        }
        else{
            return "";
        }
    }

}
