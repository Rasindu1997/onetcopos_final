package lk.onetco.fx.view.controller;

import com.jfoenix.controls.JFXTextField;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.StringConverter;
import lk.onetco.fx.dao.impl.CategoryDaoImpl;
import lk.onetco.fx.dao.impl.ItemDaoImpl;
import lk.onetco.fx.model.Category;
import lk.onetco.fx.model.Item;
import lk.onetco.fx.validation.Validation;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

public class ViewItem_Controller implements Initializable {


    private double xOffset = 0;
    private double yOffset = 0;

    @FXML
    private TextField txtSearch;

    @FXML
    private JFXTextField txtItemWarrenty;

    @FXML
    private JFXTextField txtItemName;

    @FXML
    private JFXTextField txtItemPrice;

    @FXML
    private JFXTextField txtItemQty;

    @FXML
    private Button btnUpdate;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnAdd;

    final ObservableList<Item> items = FXCollections.observableArrayList();


    final ObservableList categories = FXCollections.observableArrayList();

    @FXML
    private ComboBox<Category> comboCategory = new ComboBox<>(categories);


    @FXML
    private TableView<Item> tblItems;

    @FXML
    private TableColumn<Item, Integer> colId;

    @FXML
    private TableColumn<Item, String> colItemCode;

    @FXML
    private TableColumn<Item, String> colCategoryCode;

    @FXML
    private TableColumn<Item, String> colName;

    @FXML
    private TableColumn<Item, BigDecimal> colPrice;

    @FXML
    private TableColumn<Item, Integer> colQty;

    @FXML
    private Label lblNextItemCode;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            setTableValues();
            loadAllCategoriesToComboBox();
            setNextItemCodeToLable();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        tblItems.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Item>() {
            @Override
            public void changed(ObservableValue<? extends Item> observable, Item oldValue, Item selectedItem) {

                if (selectedItem == null) {
                    // Clear Selection
                    return;
                }
                Category cat = new Category();
                txtItemName.setText(selectedItem.getItemName());
                txtItemPrice.setText(String.valueOf(selectedItem.getRetailPrice()));
                txtItemQty.setText(String.valueOf(selectedItem.getItemStockQty()));
                txtItemWarrenty.setText(selectedItem.getItemWarranty());
                CategoryDaoImpl categoryDao = new CategoryDaoImpl();
                String categoryName = null;
                Category category = new Category();
                try {
                    categoryName = categoryDao.getCategoryNameByCategoryCode(tblItems.getSelectionModel().
                            getSelectedItem().getItemCategoryCode());
                    category.setCategoryName(categoryName);
                    category.setCategoryCode(tblItems.getSelectionModel().
                            getSelectedItem().getItemCategoryCode());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                comboCategory.getSelectionModel().select(category);
//                comboCategory.getItems().stream()
//                        .filter(category -> tblItems.getSelectionModel().getSelectedItem().getItemCategoryCode()
//                                .equals(comboCategory.getSelectionModel().getSelectedItem().getCategoryCode()))
//                        .findAny()
//                        .ifPresent(comboCategory.getSelectionModel()::select);
            }
        });
    }

    public void setTableValues() throws SQLException {
        tblItems.getItems().clear();
        ItemDaoImpl item = new ItemDaoImpl();
        ResultSet rst = item.getAllItems();
        while (rst.next()) {
            items.add(new Item(rst.getInt("item_id"), rst.getString("item_code"),
                    rst.getString("item_category_id"), rst.getString("item_name"), rst.getBigDecimal("item_retail_price"),
                    rst.getInt("item_stock_qty"),rst.getString("item_warranty")));
        }
        colId.setCellValueFactory(new PropertyValueFactory<Item, Integer>("itemId"));
        colItemCode.setCellValueFactory(new PropertyValueFactory<Item, String>("itemCode"));
        colCategoryCode.setCellValueFactory(new PropertyValueFactory<Item, String>("itemCategoryCode"));
        colName.setCellValueFactory(new PropertyValueFactory<Item, String>("itemName"));
        colPrice.setCellValueFactory(new PropertyValueFactory<Item, BigDecimal>("retailPrice"));
        colQty.setCellValueFactory(new PropertyValueFactory<Item, Integer>("itemStockQty"));
        tblItems.setItems(items);
    }

    @FXML
    void btnDeleteOnClicked(MouseEvent event) {
        if (tblItems.getSelectionModel().getSelectedItem() == null) {
            Alert confirmMsg = new Alert(Alert.AlertType.WARNING, "Please Select Item to Delete!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
        } else {
            Alert confirmMsg = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure to delete this item?", ButtonType.YES, ButtonType.NO);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();

            if (buttonType.get() == ButtonType.YES) {
                int selectedRow = tblItems.getSelectionModel().getSelectedItem().getItemId();
                tblItems.getItems().remove(tblItems.getSelectionModel().getSelectedItem());
                ItemDaoImpl item = new ItemDaoImpl();
                boolean result = false;
                try {
                    result = item.deleteItem(selectedRow);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                if (!result) {
                    new Alert(Alert.AlertType.ERROR, "Failed to delete the item", ButtonType.OK).showAndWait();
                } else {
                    try {
                        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/lk/onetco/fx/view/SuccessMessage.fxml"));
                        Parent root = null;
                        root = (Parent) fxmlLoader.load();
                        Stage stage = new Stage();
                        stage.setScene(new Scene(root));
                        stage.initStyle(StageStyle.UNDECORATED);
                        stage.show();
                    }catch(IOException e){
                        e.printStackTrace();
                    }
                    reset();
                }
            }
        }
    }

    @FXML
    void btnUpdateOnClicked(MouseEvent event) {
        if (tblItems.getSelectionModel().getSelectedItem() == null) {
            Alert confirmMsg = new Alert(Alert.AlertType.WARNING, "Please Select Item to Update!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
        } else {
            Alert confirmMsg = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure to Update this item?", ButtonType.YES, ButtonType.NO);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            if (buttonType.get() == ButtonType.YES) {
                if (!(Validation.isNotEmpty(txtItemName.getText()))) {
                    confirmMsg = new Alert(Alert.AlertType.WARNING, "Item Name is Required!", ButtonType.OK);
                    Optional<ButtonType> buttonType1 = confirmMsg.showAndWait();
                    return;
                }
                if (comboCategory.getSelectionModel().getSelectedItem().toString().isEmpty()) {
                    confirmMsg = new Alert(Alert.AlertType.WARNING, "Please Select a Category!", ButtonType.OK);
                    Optional<ButtonType> buttonType1 = confirmMsg.showAndWait();
                    return;
                }
                if (!Validation.isNotEmpty(txtItemPrice.getText())) {
                    confirmMsg = new Alert(Alert.AlertType.WARNING, "Retail Price is Required!", ButtonType.OK);
                    Optional<ButtonType> buttonType1= confirmMsg.showAndWait();
                    return;
                }

                if (!Validation.isBigDecimalValue(txtItemPrice.getText())) {
                    confirmMsg = new Alert(Alert.AlertType.WARNING, "Please Enter valid Retail Price!", ButtonType.OK);
                    Optional<ButtonType> buttonType1 = confirmMsg.showAndWait();
                    return;
                }

                if (!Validation.isNotEmpty(txtItemQty.getText())) {
                    confirmMsg = new Alert(Alert.AlertType.WARNING, "Item Qty is Required!", ButtonType.OK);
                    Optional<ButtonType> buttonType1 = confirmMsg.showAndWait();
                    return;
                }

                if (!Validation.isIntegerValue(txtItemQty.getText())) {
                    confirmMsg = new Alert(Alert.AlertType.WARNING, "Please Enter valid Item Qty!", ButtonType.OK);
                    Optional<ButtonType> buttonType1 = confirmMsg.showAndWait();
                    return;
                }

                if (!Validation.isNotEmpty(txtItemWarrenty.getText())) {
                    confirmMsg = new Alert(Alert.AlertType.WARNING, "Item warranty is Required!", ButtonType.OK);
                    Optional<ButtonType> buttonType1 = confirmMsg.showAndWait();
                    return;
                }
            Item item = new Item();
            item.setItemName(txtItemName.getText());
            item.setItemCategoryCode(comboCategory.getSelectionModel().getSelectedItem().getCategoryCode());
            item.setRetailPrice(new BigDecimal(txtItemPrice.getText()));
            item.setItemStockQty(Integer.valueOf(txtItemQty.getText()));
            item.setItemId(tblItems.getSelectionModel().getSelectedItem().getItemId());
            item.setItemWarranty(txtItemWarrenty.getText());

            ItemDaoImpl itemdao = new ItemDaoImpl();
            boolean result = false;
            try {
                result = itemdao.updateItem(item);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (!result) {
                new Alert(Alert.AlertType.ERROR, "Failed to Update the item", ButtonType.OK).showAndWait();
            } else {
                reset();
                try {
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/lk/onetco/fx/view/SuccessMessage.fxml"));
                    Parent root = (Parent) fxmlLoader.load();
                    Stage stage = new Stage();
                    stage.setScene(new Scene(root));
                    stage.initStyle(StageStyle.UNDECORATED);
                    stage.show();
                    setTableValues();
//                comboCategory.getSelectionModel().select(-1);
                } catch (SQLException | IOException e) {
                    e.printStackTrace();
                }
            }
            }
        }
    }

    private void reset() {
        txtItemName.clear();
        txtItemPrice.clear();
        tblItems.getSelectionModel().clearSelection();
        txtItemQty.clear();
        txtItemWarrenty.clear();
        comboCategory.getSelectionModel().select(-1);
        setNextItemCodeToLable();
    }

    public void clearAll() {
        txtItemName.setText(null);
        txtItemPrice.setText(null);
        txtItemQty.setText(null);
        comboCategory.getSelectionModel().select(-1);
        setNextItemCodeToLable();
    }

    @FXML
    void btnAddOnClicked(MouseEvent event) throws SQLException {
        Alert confirmMsg = null;
        if (!(Validation.isNotEmpty(txtItemName.getText()))) {
            confirmMsg = new Alert(Alert.AlertType.WARNING, "Item Name is Required!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            return;
        }
        if (comboCategory.getSelectionModel().getSelectedIndex() == -1) {
            confirmMsg = new Alert(Alert.AlertType.WARNING, "Please Select a Category!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            return;
        }
        if (!Validation.isNotEmpty(txtItemPrice.getText())) {
            confirmMsg = new Alert(Alert.AlertType.WARNING, "Retail Price is Required!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            return;
        }

        if (!Validation.isBigDecimalValue(txtItemPrice.getText())) {
            confirmMsg = new Alert(Alert.AlertType.WARNING, "Please Enter valid Retail Price!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            return;
        }

        if (!Validation.isNotEmpty(txtItemQty.getText())) {
            confirmMsg = new Alert(Alert.AlertType.WARNING, "Item Qty is Required!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            return;
        }

        if (!Validation.isIntegerValue(txtItemQty.getText())) {
            confirmMsg = new Alert(Alert.AlertType.WARNING, "Please Enter valid Item Qty!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            return;
        }

        ItemDaoImpl itemDao = new ItemDaoImpl();
        Item item = new Item();
        item.setItemName(txtItemName.getText());
        item.setItemCategoryCode(comboCategory.getSelectionModel().getSelectedItem().getCategoryCode());
        item.setRetailPrice(new BigDecimal(txtItemPrice.getText()));
        item.setItemStockQty(Integer.valueOf(txtItemQty.getText()));
        item.setItemWarranty(txtItemWarrenty.getText());
        if (itemDao.addItem(item)) {
            try{
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/lk/onetco/fx/view/SuccessMessage.fxml"));
                Parent root = (Parent) fxmlLoader.load();
                Stage stage = new Stage();
                stage.setScene(new Scene(root));
                stage.initStyle(StageStyle.UNDECORATED);
                stage.show();
            }catch (IOException e) {
                e.printStackTrace();
            }
            clearAll();
        }
        else{
            new Alert(Alert.AlertType.ERROR, "Failed to Add the item", ButtonType.OK).showAndWait();
        }
        setTableValues();
    }

    @FXML
    void txtSearchOnKeyReleased(KeyEvent event) throws SQLException {
        tblItems.getItems().clear();
        ItemDaoImpl itemDao = new ItemDaoImpl();
        ResultSet rst = null;
        try {
            rst = itemDao.findItems(txtSearch.getText());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        while (rst.next()) {
            items.add(new Item(rst.getInt("item_id"), rst.getString("item_code"),
                    rst.getString("item_category_id"), rst.getString("item_name"), rst.getBigDecimal("item_retail_price"),
                    rst.getInt("item_stock_qty"),rst.getString("item_warranty")));
        }
        colId.setCellValueFactory(new PropertyValueFactory<Item, Integer>("itemId"));
        colItemCode.setCellValueFactory(new PropertyValueFactory<Item, String>("itemCode"));
        colCategoryCode.setCellValueFactory(new PropertyValueFactory<Item, String>("itemCategoryCode"));
        colName.setCellValueFactory(new PropertyValueFactory<Item, String>("itemName"));
        colPrice.setCellValueFactory(new PropertyValueFactory<Item, BigDecimal>("retailPrice"));
        colQty.setCellValueFactory(new PropertyValueFactory<Item, Integer>("itemStockQty"));
        tblItems.setItems(items);
    }


    public void loadAllCategoriesToComboBox() throws SQLException {
        CategoryDaoImpl category = new CategoryDaoImpl();
        ResultSet rst = category.getAllCategory();
        while (rst.next()) {
            categories.addAll(new Category(rst.getString("category_code"), rst.getString("category_name")));
        }
        comboCategory.setMaxHeight(30);

        comboCategory.setConverter(new StringConverter<Category>() {
            @Override
            public String toString(Category object) {
                return object.getCategoryName();
            }

            @Override
            public Category fromString(String string) {
                return comboCategory.getItems().stream().filter(ap ->
                        ap.getCategoryName().equals(string)).findFirst().orElse(null);
            }
        });
        comboCategory.setItems(categories);

        rst.close();
    }

    public void setNextItemCodeToLable() {
        try {
            lblNextItemCode.setText(ItemDaoImpl.getNextItemCode());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
