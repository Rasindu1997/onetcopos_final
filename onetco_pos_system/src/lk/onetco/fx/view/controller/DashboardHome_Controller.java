package lk.onetco.fx.view.controller;

import javafx.animation.ScaleTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import lk.onetco.fx.dao.impl.CommonDaoImpl;
import lk.onetco.fx.model.CommonModel;
import lk.onetco.fx.validation.Validation;

import java.math.BigDecimal;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class DashboardHome_Controller implements Initializable {

    @FXML
    private Label lblOrderCount;

    @FXML
    private Label lblCustomerCount;

    @FXML
    private Label lblItemsCount;

    @FXML
    private Label lblTotalIncome;

    @FXML
    private TableView<CommonModel> tblRecentInvoices;

    @FXML
    private TableColumn<CommonModel, String> colInvoiceNo;

    @FXML
    private TableColumn<CommonModel, String> colCustomerName;

    @FXML
    private TableColumn<CommonModel, String> colContactNo;

    @FXML
    private TableColumn<CommonModel, BigDecimal> colnvoiceTotal;

    final ObservableList<CommonModel> recentInvoices = FXCollections.observableArrayList();

    CommonDaoImpl commonDao =new CommonDaoImpl();
    int invoiceCount=0;
    int customerCount=0;
    int itemCount=0;
    BigDecimal totalIncome=BigDecimal.ZERO;

    public void setTableValues() throws SQLException {
        tblRecentInvoices.getItems().clear();
        ResultSet rst = commonDao.getTodayInvoices();
        while (rst.next()) {
            recentInvoices.add(new CommonModel(rst.getString("invoice_code"),rst.getString("customer_firstname"),
                    rst.getString("customer_contactno"),rst.getBigDecimal("invoice_total_price")));
        }
        colInvoiceNo.setCellValueFactory(new PropertyValueFactory<CommonModel,String>("invoiceCode"));
        colCustomerName.setCellValueFactory(new PropertyValueFactory<CommonModel, String>("customerName"));
        colContactNo.setCellValueFactory(new PropertyValueFactory<CommonModel, String>("contactNo"));
        colnvoiceTotal.setCellValueFactory(new PropertyValueFactory<CommonModel, BigDecimal>("invoiceTotal"));
        tblRecentInvoices.setItems(recentInvoices);
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            setTableValues();
            invoiceCount=commonDao.getInvoiceCount();
            customerCount=commonDao.getCustomersCount();
            itemCount=commonDao.getItemCount();
            totalIncome=commonDao.getIncome();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        lblOrderCount.setText(Integer.toString(invoiceCount));
        lblCustomerCount.setText(Integer.toString(customerCount));
        lblItemsCount.setText(Integer.toString(itemCount));
        lblTotalIncome.setText(totalIncome.toString());
    }

    @FXML
    void ordersBtnPlayMouseEnterAnimation(MouseEvent event) {
        if (event.getSource() instanceof Pane) {
            Pane icon = (Pane) event.getSource();

            ScaleTransition scaleT = new ScaleTransition(Duration.millis(200), icon);
            scaleT.setToX(1.06);
            scaleT.setToY(1.06);
            scaleT.play();

            DropShadow glow = new DropShadow();
            glow.setColor(Color.SKYBLUE);
            glow.setWidth(20);
            glow.setHeight(20);
            glow.setRadius(20);
            icon.setEffect(glow);
        }
    }

    @FXML
    void ordersBtnPlayMouseExitAnimation(MouseEvent event) {
        if (event.getSource() instanceof Pane){
            Pane icon = (Pane) event.getSource();
            ScaleTransition scaleT =new ScaleTransition(Duration.millis(200), icon);
            scaleT.setToX(1);
            scaleT.setToY(1);
            scaleT.play();

            icon.setEffect(null);
        }
    }

    @FXML
    void customersBtnPlayMouseEnterAnimation(MouseEvent event) {
        if (event.getSource() instanceof Pane) {
            Pane icon = (Pane) event.getSource();

            ScaleTransition scaleT = new ScaleTransition(Duration.millis(200), icon);
            scaleT.setToX(1.06);
            scaleT.setToY(1.06);
            scaleT.play();

            DropShadow glow = new DropShadow();
            glow.setColor(Color.ORANGE);
            glow.setWidth(20);
            glow.setHeight(20);
            glow.setRadius(20);
            icon.setEffect(glow);
        }
    }

    @FXML
    void customersBtnPlayMouseExitAnimation(MouseEvent event) {
            if (event.getSource() instanceof Pane){
                Pane icon = (Pane) event.getSource();
                ScaleTransition scaleT =new ScaleTransition(Duration.millis(200), icon);
                scaleT.setToX(1);
                scaleT.setToY(1);
                scaleT.play();

                icon.setEffect(null);
            }
    }

    @FXML
    void incomeBtnPlayMouseEnterAnimation(MouseEvent event) {
            if (event.getSource() instanceof Pane) {
                Pane icon = (Pane) event.getSource();

                ScaleTransition scaleT = new ScaleTransition(Duration.millis(200), icon);
                scaleT.setToX(1.06);
                scaleT.setToY(1.06);
                scaleT.play();

                DropShadow glow = new DropShadow();
                glow.setColor(Color.GREEN);
                glow.setWidth(20);
                glow.setHeight(20);
                glow.setRadius(20);
                icon.setEffect(glow);
            }
    }

    @FXML
    void incomeBtnPlayMouseExitAnimation(MouseEvent event) {
                if (event.getSource() instanceof Pane){
                    Pane icon = (Pane) event.getSource();
                    ScaleTransition scaleT =new ScaleTransition(Duration.millis(200), icon);
                    scaleT.setToX(1);
                    scaleT.setToY(1);
                    scaleT.play();

                    icon.setEffect(null);
                }
    }

    @FXML
    void itemsBtnPlayMouseEnterAnimation(MouseEvent event) {
        if (event.getSource() instanceof Pane) {
            Pane icon = (Pane) event.getSource();

            ScaleTransition scaleT = new ScaleTransition(Duration.millis(200), icon);
            scaleT.setToX(1.06);
            scaleT.setToY(1.06);
            scaleT.play();

            DropShadow glow = new DropShadow();
            glow.setColor(Color.PURPLE);
            glow.setWidth(20);
            glow.setHeight(20);
            glow.setRadius(20);
            icon.setEffect(glow);
        }
    }

    @FXML
    void itemsBtnPlayMouseExitAnimation(MouseEvent event) {
        if (event.getSource() instanceof Pane){
            Pane icon = (Pane) event.getSource();
            ScaleTransition scaleT =new ScaleTransition(Duration.millis(200), icon);
            scaleT.setToX(1);
            scaleT.setToY(1);
            scaleT.play();

            icon.setEffect(null);
        }
    }
}
