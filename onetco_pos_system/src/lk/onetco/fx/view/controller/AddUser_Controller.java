package lk.onetco.fx.view.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lk.onetco.fx.dao.UserDao;
import lk.onetco.fx.dao.impl.UserDaoImpl;
import lk.onetco.fx.database.DBConnection;
import lk.onetco.fx.model.User;
import lk.onetco.fx.validation.Validation;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;
import java.util.ResourceBundle;

public class AddUser_Controller implements Initializable {

    ObservableList<String> userPrivilege = FXCollections.observableArrayList("Admin", "User");

    private String userType;

    @FXML
    private TextField uid;

    @FXML
    private TextField uname;

    @FXML
    private Button addUserButton;

    @FXML
    private PasswordField pwd;

    @FXML
    private RadioButton adminRadio;

    @FXML
    private TableView<User> userTable;

    @FXML
    private TableColumn<User, Integer> col_uid;

    @FXML
    private TableColumn<User, String> col_uname;

    @FXML
    private TableColumn<User, String> col_pwd;

    @FXML
    private TableColumn<User, String> col_utype;

    @FXML
    private Label infoText;

    @FXML
    private CheckBox adminBox;

    @FXML
    private TextField text_uid;

    @FXML
    private Button updateButton;

    @FXML
    private Button deleteButton;

    @FXML
    private ChoiceBox<String> userPrivi;

    @FXML
    private Label lblNextUserId;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {
            showUsers();
            setNextUserIdToLable();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        userPrivi.setValue("Admin");
        userPrivi.setItems(userPrivilege);
    }

    @FXML
    void deleteButtonClicked(MouseEvent event) throws SQLException, IOException {
/*
        if (uname.getText().isEmpty() || pwd.getText().isEmpty() || userPrivi.getSelectionModel().getSelectedItem().isEmpty()) {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/lk/onetco/fx/view/ErrorMessage.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.initStyle(StageStyle.UNDECORATED);
            stage.show();
        }
        else {
        */
        if (userTable.getSelectionModel().getSelectedItem() == null) {
            Alert confirmMsg = new Alert(Alert.AlertType.WARNING, "Please Select User to Delete!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
        } else {
            Alert confirmMsg = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure to delete this item?", ButtonType.YES, ButtonType.NO);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();

            if (buttonType.get() == ButtonType.YES) {
                int user_id = Integer.parseInt(lblNextUserId.getText());

                UserDaoImpl userDao = new UserDaoImpl();
                userDao.deleteUser(user_id);
                showUsers();
            }

            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/lk/onetco/fx/view/SuccessMessage.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.initStyle(StageStyle.UNDECORATED);
            stage.show();
        }
    }


    @FXML
    void addUserButtonClicked(MouseEvent event) throws SQLException, IOException {


        if (!(Validation.isNotEmpty(uname.getText().trim()))) {
            Alert confirmMsg = new Alert(Alert.AlertType.WARNING, "User name is Required!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            return;
        }
        if (!(Validation.isNotEmpty(pwd.getText().trim()))) {
            Alert confirmMsg = new Alert(Alert.AlertType.WARNING, "Password is Required!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            return;
        }
        if (userPrivi.getSelectionModel().getSelectedItem().isEmpty()) {
            Alert confirmMsg = new Alert(Alert.AlertType.WARNING, "Please Select User Priviledge!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            return;
        }

        if(UserDaoImpl.isCustomerNameExist(uname.getText().toString())){
            Alert confirmMsg = new Alert(Alert.AlertType.WARNING, "Username Already Exists!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            return;
        }

         else {
            String name = uname.getText();
            String pass = pwd.getText();
            userType = userPrivi.getSelectionModel().getSelectedItem();

            User user = new User();
            user.setUsername(name);
            user.setPassword(userType.equals("Admin") ? "admin":"user");
            user.setUserType(userType);
            user.setUserPasswordStatus(User.notLogged);

            UserDaoImpl userDao = new UserDaoImpl();
            userDao.addUser(user);
            showUsers();

            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/lk/onetco/fx/view/SuccessMessage.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.initStyle(StageStyle.UNDECORATED);
            stage.show();
        }

    }


    @FXML
    void updateButtonClicked(MouseEvent event) throws SQLException, IOException {
        /*
        if (uname.getText().isEmpty() || pwd.getText().isEmpty() || userPrivi.getSelectionModel().getSelectedItem().isEmpty()) {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/lk/onetco/fx/view/ErrorMessage.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.initStyle(StageStyle.UNDECORATED);
            stage.show();
        }
        */
        if (userTable.getSelectionModel().getSelectedItem() == null) {
            Alert confirmMsg = new Alert(Alert.AlertType.WARNING, "Please Select User to Update!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
        } else {
            Alert confirmMsg = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure to Update this user?", ButtonType.YES, ButtonType.NO);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();

            if (buttonType.get() == ButtonType.YES) {

                if (!(Validation.isNotEmpty(uname.getText().trim()))) {
                    Alert confirmMsg1 = new Alert(Alert.AlertType.WARNING, "User name is Required!", ButtonType.OK);
                    Optional<ButtonType> buttonType1 = confirmMsg1.showAndWait();
                    return;
                }
                if (!(Validation.isNotEmpty(pwd.getText().trim()))) {
                    Alert confirmMsg1 = new Alert(Alert.AlertType.WARNING, "Password is Required!", ButtonType.OK);
                    Optional<ButtonType> buttonType1 = confirmMsg1.showAndWait();
                    return;
                }
                if (userPrivi.getSelectionModel().getSelectedItem().isEmpty()) {
                    Alert confirmMsg1 = new Alert(Alert.AlertType.WARNING, "Please Select User Priviledge!", ButtonType.OK);
                    Optional<ButtonType> buttonType1 = confirmMsg.showAndWait();
                    return;
                }

                String name = uname.getText();
                String pass = pwd.getText();
                int user_id = Integer.parseInt(lblNextUserId.getText());
                userType = userPrivi.getSelectionModel().getSelectedItem();

                User user = new User();
                user.setUsername(name);
                user.setUserid(user_id);
                user.setPassword(pass);
                user.setUserType(userType);

                UserDaoImpl userDao = new UserDaoImpl();
                userDao.updateUser(user);
                showUsers();

                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/lk/onetco/fx/view/SuccessMessage.fxml"));
                Parent root = (Parent) fxmlLoader.load();
                Stage stage = new Stage();
                stage.setScene(new Scene(root));
                stage.initStyle(StageStyle.UNDECORATED);
                stage.show();
            }
        }

    }


    public ObservableList<User> getUsers() throws SQLException {

        ObservableList<User> users = FXCollections.observableArrayList();

        Connection connection = null;
        try {
            connection = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String Query = "SELECT * FROM user";
        Statement statement;
        ResultSet resultset;
        User user;

        statement = connection.createStatement();
        resultset = statement.executeQuery(Query);

        while (resultset.next()) {
            user = new User(resultset.getInt("userid"), resultset.getString("username"), resultset.getString("password"),
                    resultset.getString("usertype"));
            users.add(user);
        }
        return users;

    }

    public void showUsers() throws SQLException {
        ObservableList<User> list = getUsers();
        col_uid.setCellValueFactory(new PropertyValueFactory<User, Integer>("userid"));
        col_uname.setCellValueFactory(new PropertyValueFactory<User, String>("username"));
        col_pwd.setCellValueFactory(new PropertyValueFactory<User, String>("password"));
        col_utype.setCellValueFactory(new PropertyValueFactory<User, String>("userType"));
        userTable.setItems(list);
        setNextUserIdToLable();
    }


    @FXML
    void tableViewClicked(MouseEvent event) {

        User user = userTable.getSelectionModel().getSelectedItem();
        lblNextUserId.setText(String.valueOf(user.getUserid()));
        uname.setText(user.getUsername());
        pwd.setText(user.getPassword());
        userPrivi.setValue(user.getUserType());

    }

    public void setNextUserIdToLable() {
        try {
            lblNextUserId.setText(Integer.toString(UserDaoImpl.getNextUserId()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
