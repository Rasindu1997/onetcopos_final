package lk.onetco.fx.view.controller;

import javafx.animation.ScaleTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import lk.onetco.fx.dao.impl.CategoryDaoImpl;
import lk.onetco.fx.database.DBConnection;
import lk.onetco.fx.model.Category;
import lk.onetco.fx.validation.Validation;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;
import java.util.ResourceBundle;

public class AddCategoryController implements Initializable {

    private double xOffset = 0;
    private double yOffset = 0;


    @FXML
    private TextField txt_id;

    @FXML
    private TextField txt_code;

    @FXML
    private TextField txt_name;

    @FXML
    private Button addCategory;

    @FXML
    private TableView<Category> categoryTable;

    @FXML
    private TableColumn<Category, Integer> col_id;

    @FXML
    private TableColumn<Category, String> col_code;

    @FXML
    private TableColumn<Category, String> col_name;

    @FXML
    private Button updateButton;

    @FXML
    private Button deleteButton;

    @FXML
    private ImageView imgClose;

    @FXML
    private ImageView imgMinimize;

    @FXML
    private Label lblMenu;

    @FXML
    private Label lblNextCategoryId;


    @FXML
    void playMouseEnterAnimation(MouseEvent event) {
        if (event.getSource() instanceof ImageView) {
            ImageView icon = (ImageView) event.getSource();

            switch (icon.getId()) {
                case "imgStockManagement":
                    //lblMenu.setText("Stock Management");
                    break;
                case "imgUserManagement":
                    // lblMenu.setText("User Management");
                    break;
                case "imgInvoice":
                    // lblMenu.setText("InvoiceDao Details");
                    break;
                case "imgUser":
                    //lblMenu.setText("User Details");
                    break;
                case "imgReports":
                    // lblMenu.setText("Reports");
                    break;
                case "categoryManagement":
                    // lblMenu.setText("Manage Categories");
            }

            ScaleTransition scaleT = new ScaleTransition(Duration.millis(200), icon);
            scaleT.setToX(1.2);
            scaleT.setToY(1.2);
            scaleT.play();

            DropShadow glow = new DropShadow();
            glow.setColor(Color.SKYBLUE);
            glow.setWidth(20);
            glow.setHeight(20);
            glow.setRadius(20);
            icon.setEffect(glow);
        }
    }

    @FXML
    void playMouseExitAnimation(MouseEvent event) {
        if (event.getSource() instanceof ImageView) {
            ImageView icon = (ImageView) event.getSource();
            ScaleTransition scaleT = new ScaleTransition(Duration.millis(200), icon);
            scaleT.setToX(1);
            scaleT.setToY(1);
            scaleT.play();

            icon.setEffect(null);
            //lblMenu.setText("Welcome");
        }
    }

    @FXML
    void imgCloseMouseClicked(MouseEvent event) {
        Stage stage = (Stage) imgClose.getScene().getWindow();
        stage.close();
    }

    @FXML
    void imgMinimizeMouseClicked(MouseEvent event) {
        Stage stage = (Stage) imgClose.getScene().getWindow();
        stage.setIconified(true);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {


        try {
            setNextCategoryIdToLable();
            showCategories();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @FXML
    void addCategoryButtonClicked(MouseEvent event) throws SQLException {
        Alert confirmMsg = null;
        if (!(Validation.isNotEmpty(txt_code.getText()))) {
            confirmMsg = new Alert(Alert.AlertType.WARNING, "Category Code is Required!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            return;
        }
        if (!(Validation.isNotEmpty(txt_name.getText()))) {
            confirmMsg = new Alert(Alert.AlertType.WARNING, "Category Name is Required!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            return;
        }
        String code = txt_code.getText();
        String category_name = txt_name.getText();

        Category category = new Category();
        category.setCategoryCode(code);
        category.setCategoryName(category_name);

        CategoryDaoImpl categoryDao = new CategoryDaoImpl();
        if (categoryDao.addCategory(category)) {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/lk/onetco/fx/view/SuccessMessage.fxml"));
                Parent root = (Parent) fxmlLoader.load();
                Stage stage = new Stage();
                stage.setScene(new Scene(root));
                stage.initStyle(StageStyle.UNDECORATED);
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
            showCategories();
            clearForm();

        } else {
            new Alert(Alert.AlertType.ERROR, "Failed to Add the Category", ButtonType.OK).showAndWait();
        }
    }

    public ObservableList<Category> getCategories() throws SQLException {

        ObservableList<Category> categories = FXCollections.observableArrayList();

        Connection connection = null;
        try {
            connection = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String Query = "SELECT * FROM category";
        Statement statement;
        ResultSet resultset;
        Category category;

        statement = connection.createStatement();
        resultset = statement.executeQuery(Query);

        while (resultset.next()) {
            category = new Category(resultset.getInt("category_id"), resultset.getString("category_code"), resultset.getString("category_name"));
            categories.add(category);
        }
        return categories;

    }

    public void showCategories() throws SQLException {
        ObservableList<Category> list = getCategories();

        col_id.setCellValueFactory(new PropertyValueFactory<Category, Integer>("categoryId"));
        col_code.setCellValueFactory(new PropertyValueFactory<Category, String>("categoryCode"));
        col_name.setCellValueFactory(new PropertyValueFactory<Category, String>("categoryName"));


        categoryTable.setItems(list);
    }

    @FXML
    void tableViewClicked(MouseEvent event) {

        Category category = categoryTable.getSelectionModel().getSelectedItem();
        txt_code.setText(category.getCategoryCode());
        txt_name.setText(category.getCategoryName());

    }

    @FXML
    void deleteButtonClicked(MouseEvent event) throws SQLException {
        if (categoryTable.getSelectionModel().getSelectedItem() == null) {
            Alert confirmMsg = new Alert(Alert.AlertType.WARNING, "Please Select Category to Delete!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
        } else {
            Alert confirmMsg = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure to delete this Category?", ButtonType.YES, ButtonType.NO);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();

            if (buttonType.get() == ButtonType.YES) {
                int category_id = categoryTable.getSelectionModel().getSelectedItem().getCategoryId();
                CategoryDaoImpl categoryDao = new CategoryDaoImpl();
                categoryDao.deleteCategory(category_id);
                try {
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/lk/onetco/fx/view/SuccessMessage.fxml"));
                    Parent root = (Parent) fxmlLoader.load();
                    Stage stage = new Stage();
                    stage.setScene(new Scene(root));
                    stage.initStyle(StageStyle.UNDECORATED);
                    stage.show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                showCategories();
                clearForm();

            }
        }
    }

    @FXML
    void updateButtonClicked(MouseEvent event) throws SQLException {
        if (categoryTable.getSelectionModel().getSelectedItem() == null) {
            Alert confirmMsg = new Alert(Alert.AlertType.WARNING, "Please Select Category to Update!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
        } else {
            Alert confirmMsg = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure to Update this Category?", ButtonType.YES, ButtonType.NO);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();

            if (buttonType.get() == ButtonType.YES) {
                if (!(Validation.isNotEmpty(txt_code.getText()))) {
                    confirmMsg = new Alert(Alert.AlertType.WARNING, "Category Code is Required!", ButtonType.OK);
                    Optional<ButtonType> buttonType1 = confirmMsg.showAndWait();
                    return;
                }
                if (!(Validation.isNotEmpty(txt_name.getText()))) {
                    confirmMsg = new Alert(Alert.AlertType.WARNING, "Category Name is Required!", ButtonType.OK);
                    Optional<ButtonType> buttonType1 = confirmMsg.showAndWait();
                    return;
                }
                String code = txt_code.getText();
                String name = txt_name.getText();
                int id = Integer.parseInt(lblNextCategoryId.getText());

                Category category = new Category();
                category.setCategoryId(id);
                category.setCategoryName(name);
                category.setCategoryCode(code);

                CategoryDaoImpl categoryDao = new CategoryDaoImpl();
                if (categoryDao.updateCategory(category)) {
                    try {
                        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/lk/onetco/fx/view/SuccessMessage.fxml"));
                        Parent root = (Parent) fxmlLoader.load();
                        Stage stage = new Stage();
                        stage.setScene(new Scene(root));
                        stage.initStyle(StageStyle.UNDECORATED);
                        stage.show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    showCategories();
                    clearForm();
                } else {
                    new Alert(Alert.AlertType.ERROR, "Failed to Update the Category", ButtonType.OK).showAndWait();
                }
            }
        }
    }

    @FXML
    public void clearForm() {
        txt_name.setText("");
        txt_code.setText("");
        setNextCategoryIdToLable();
    }

    public void setNextCategoryIdToLable() {
        try {
            lblNextCategoryId.setText(Integer.toString(CategoryDaoImpl.getNextCategoryCode()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
