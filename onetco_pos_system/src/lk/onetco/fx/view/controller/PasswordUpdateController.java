package lk.onetco.fx.view.controller;

import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lk.onetco.fx.dao.impl.UserDaoImpl;
import lk.onetco.fx.validation.Validation;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

public class PasswordUpdateController implements Initializable {
    public static String username="";
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
    @FXML
    private JFXTextField txtPassword;

    @FXML
    private Button btnUpdate;

    @FXML
    void updatePasswordButtonClicked(MouseEvent event) {
        try {
            int usedId=UserDaoImpl.getUserID(username);
            UserDaoImpl userDao = new UserDaoImpl();
            if (!(Validation.isNotEmpty(txtPassword.getText()))) {
                Alert confirmMsg = new Alert(Alert.AlertType.WARNING, "Password is Required!", ButtonType.OK);
                Optional<ButtonType> buttonType = confirmMsg.showAndWait();
                return;
            }
            if(userDao.updateUserPassword(txtPassword.getText().trim(),usedId)){
                FXMLLoader fxmlLoader =  new FXMLLoader(getClass().getResource("/lk/onetco/fx/view/Login.fxml"));
                Parent root=(Parent) fxmlLoader.load();
                Stage stage = new Stage();
                stage.setScene(new Scene(root));
                stage.initStyle(StageStyle.UNDECORATED);
                stage.show();
                ((Node)(event.getSource())).getScene().getWindow().hide();
            }
            else{
                Alert confirmMsg = new Alert(Alert.AlertType.WARNING, "Password is not updated!", ButtonType.OK);
                Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }


}
