package lk.onetco.fx.view.controller;

import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import lk.onetco.fx.dao.impl.*;
import lk.onetco.fx.database.DBConnection;
import lk.onetco.fx.model.*;
import lk.onetco.fx.validation.Validation;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

public class PlaceOrder_Controller implements Initializable {

    public  static ObservableList<Cart> cartItems = FXCollections.observableArrayList();
    public  static BigDecimal invoicTotalPrice=BigDecimal.ZERO;
    public static Boolean isReArrangedOrder=false;
    public static int invoiceId=0;


    @FXML
    private VBox vbMenu;

    FileChooser fileChooser = new FileChooser();

    @FXML
    private CheckBox chkBtnTerms;

    @FXML
    private TextField fldFirstName;

    @FXML
    private TextField fldLastName;

    @FXML
    private TextField fldContactNo;

    @FXML
    private Label lblTotalAmount;

    @FXML
    private TextField fldPaidAmount;

    @FXML
    private Button btnSaveInvoice;

    @FXML
    private Label lblBalance;

    @FXML
    private TextArea fldRemark;

    @FXML
    private Button btnPlaceOrder;

    @FXML
    private ImageView imgClose;

    @FXML
    private ImageView imgMinimize;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        lblTotalAmount.setText(invoicTotalPrice.toString());
        Platform.runLater(()->fldPaidAmount.requestFocus());
//        fldPaidAmount.requestFocus();
//        lblBalance.setText(findBalance().toString());
        fldPaidAmount.setOnKeyPressed(ke -> {
            if (ke.getCode().equals(KeyCode.ENTER))
            {
                lblBalance.setText(findBalance().toString());
            }
        });

        fldPaidAmount.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            focusState(newValue);
        });
    }

    @FXML
    void imgCloseMouseClicked(MouseEvent event) {
        Stage stage = (Stage) imgClose.getScene().getWindow();
        stage.hide();
    }
    @FXML
    void btnPlaceOrderMouseClicked(MouseEvent event) {
        Alert confirmMsg = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure to Place this Order?", ButtonType.YES, ButtonType.NO);
        Optional<ButtonType> buttonType = confirmMsg.showAndWait();
        if (buttonType.get() == ButtonType.YES) {
            if (!Validation.isNotEmpty(fldPaidAmount.getText())) {
                confirmMsg = new Alert(Alert.AlertType.WARNING, "Retail Price is Required!", ButtonType.OK);
                Optional<ButtonType> buttonType1= confirmMsg.showAndWait();
                return;
            }

            if (!Validation.isBigDecimalValue(fldPaidAmount.getText())) {
                confirmMsg = new Alert(Alert.AlertType.WARNING, "Please Enter valid Retail Price!", ButtonType.OK);
                Optional<ButtonType> buttonType1 = confirmMsg.showAndWait();
                return;
            }
            if (!(Validation.isNotEmpty(fldFirstName.getText()))) {
                confirmMsg = new Alert(Alert.AlertType.WARNING, "First Name is Required!", ButtonType.OK);
                Optional<ButtonType> buttonType1 = confirmMsg.showAndWait();
                return;
            }
            if (!(Validation.isNotEmpty(fldLastName.getText()))) {
                confirmMsg = new Alert(Alert.AlertType.WARNING, "Last Price is Required!", ButtonType.OK);
                Optional<ButtonType> buttonType1 = confirmMsg.showAndWait();
                return;
            }
            if (!(Validation.isNotEmpty(fldContactNo.getText()))) {
                confirmMsg = new Alert(Alert.AlertType.WARNING, "Contact No is Required!", ButtonType.OK);
                Optional<ButtonType> buttonType1 = confirmMsg.showAndWait();
                return;
            }
            /*
            if (!(Validation.isNotEmpty(fldLastName.getText()))) {
                confirmMsg = new Alert(Alert.AlertType.WARNING, "Remark is Required!", ButtonType.OK);
                Optional<ButtonType> buttonType1 = confirmMsg.showAndWait();
                return;
            }
            */

            if(!(chkBtnTerms.isSelected())){
                confirmMsg = new Alert(Alert.AlertType.WARNING, "Please Accept Terms And Conditions!", ButtonType.OK);
                Optional<ButtonType> buttonType1 = confirmMsg.showAndWait();
                return;
            }

            Invoice invoice = new Invoice();
            InvoiceDetails invoiceDetails = new InvoiceDetails();
            Payment payment = new Payment();
            Customer customer = new Customer();
            InvoiceDaoImpl invoiceDao = new InvoiceDaoImpl();
            InvoiceDetailsDaoImpl invoiceDetailsDao = new InvoiceDetailsDaoImpl();
            CustomerDaoImpl customerDao = new CustomerDaoImpl();
            PaymentDaoImpl paymentDao = new PaymentDaoImpl();
            ItemDaoImpl itemDao = new ItemDaoImpl();

            int i = 0;
            int totalQty = 0;
            BigDecimal discountedPrice = BigDecimal.ZERO;
            while (i < cartItems.size()) {
                totalQty += cartItems.get(i).getQty();
                discountedPrice = discountedPrice.add(cartItems.get(i).getDiscount());
                invoice.setInvoiceItemId(cartItems.get(i).getItemId());
                invoice.setInvoiceItemQty(totalQty);
                invoice.setInvoiceItemPrice(cartItems.get(i).getRetailPrice());
                invoice.setInvoicetemDiscount(cartItems.get(i).getDiscount());
                invoice.setInvoiceTotalPrice(invoicTotalPrice);
                i++;
            }
            try {
                invoiceDao.addInvoice(invoice,isReArrangedOrder,invoiceId);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            int k = 0;
            while (k < cartItems.size()) {

                invoiceDetails.setInvoiceId(findNextInvoiceId()-1);
                invoiceDetails.setInvoiceDetailsItemId(cartItems.get(k).getItemId());
                invoiceDetails.setInvoiceDetailsItemName(cartItems.get(k).getItemName());
                invoiceDetails.setInvoiceDetailsItemPrice(cartItems.get(k).getRetailPrice());
                invoiceDetails.setInvoiceDetailsDate(new Date());
                invoiceDetails.setInvoiceDetialsItemQty(cartItems.get(k).getQty());
                invoiceDetails.setInvoiceDetailsItemDiscount(cartItems.get(k).getDiscount());
                try {
                    if(invoiceDetailsDao.addInvoiceDetails(invoiceDetails)) {
                        itemDao.reduceItemQty(cartItems.get(k).getItemId(), cartItems.get(k).getQty());
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                k++;
            }
            customer.setFirstname(fldFirstName.getText());
            customer.setLastname(fldLastName.getText());
            customer.setContactNo(fldContactNo.getText());
            payment.setPaymentInvoiceId(findNextInvoiceId()-1);
            payment.setPaymentTotal(Validation.getBigDecimalValueFromString(fldPaidAmount.getText()));
            payment.setPaymentDiscountedPrice(invoicTotalPrice);
            payment.setPaymentBalance(findBalance());
            payment.setPaymentRemark(fldRemark.getText());

            try {
                customerDao.addCustomer(customer);
                payment.setPaymentCustomerId(findNextCustomerId()-1);
                paymentDao.addPayment(payment);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/lk/onetco/fx/view/SuccessMessage.fxml"));
                Parent root = (Parent) fxmlLoader.load();
                Stage stage = new Stage();
                stage.setScene(new Scene(root));
                stage.initStyle(StageStyle.UNDECORATED);
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    @FXML
    void btnSaveInvoiceMouseClicked(MouseEvent event) {
        Alert confirmMsg = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure to Save this Invoice as PDF?", ButtonType.YES, ButtonType.NO);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            if (buttonType.get() == ButtonType.YES) {
        try {
            int id = findNextInvoiceId() - 1;
            String nextInvoiceId = Integer.toString(id);
            Map<String, Object> map = new HashMap<>();
            map.put("invoiceId", nextInvoiceId);
            JasperDesign jasperDesign = JRXmlLoader.load(this.getClass().getResourceAsStream("/lk/onetco/fx/reports/Invoice.jrxml"));
            Connection con = DBConnection.getConnection();
            JasperReport compileReport = JasperCompileManager.compileReport(jasperDesign);
            JasperPrint jasperPrint = JasperFillManager.fillReport(compileReport, map, con);
//       JasperViewer.viewReport(jasperPrint);
            String fileDir = saveFileDirectoryPicker();
            if(!(Validation.isNotEmpty(fileDir))){
                new Alert(Alert.AlertType.WARNING, "Please Select a folder to save Report!", ButtonType.OK).showAndWait();
                return;
            }
            JasperExportManager.exportReportToPdfFile(jasperPrint, fileDir + "\\Invoice_" + new SimpleDateFormat("yyyy-MM-dd-HH-mm'.txt'").format(new Date()) + ".pdf");
        } catch (JRException e1) {
            e1.printStackTrace();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }
    }


    public BigDecimal findBalance(){
        BigDecimal balance=Validation.getBigDecimalValueFromString(fldPaidAmount.getText()).subtract(invoicTotalPrice);
        return balance;
    }

    private void focusState(boolean value) {
        if (!value) {
            fldFirstName.requestFocus();
            lblBalance.setText(findBalance().toString());
        }
    }


    public int findNextInvoiceId() {
        int nextid=0;
            InvoiceDaoImpl invoiceDao=new InvoiceDaoImpl();
        try {
            nextid=invoiceDao.getNextInvoiceId();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nextid;
    }
    public int findNextCustomerId() {
        int nextid=0;
        CustomerDaoImpl customerDao=new CustomerDaoImpl();
        try {
            nextid=customerDao.getNextCustomerId();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nextid;
    }

    public String saveFileDirectoryPicker(){
        Window stage = vbMenu.getScene().getWindow(); //get a handle to the stage
        fileChooser.setTitle("Save File"); //set the title of the Dialog window
        String defaultSaveName = "Invoice_"+new SimpleDateFormat("yyyyMMddHHmm'.txt'").format(new Date());
        fileChooser.setInitialFileName(defaultSaveName);
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PDF Files", "*.pdf"),
                new FileChooser.ExtensionFilter("Text Files", "*.txt"));
        try
        {
            File file = fileChooser.showSaveDialog(stage);
            if (file != null)
            {
                File dir = file.getParentFile();//gets the selected directory
                fileChooser.setInitialDirectory(dir);
            }
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
        if(fileChooser.getInitialDirectory()!=null){
            return fileChooser.getInitialDirectory().getAbsolutePath();
        }
        else{
            return "";
        }
    }
}
