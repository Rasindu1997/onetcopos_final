package lk.onetco.fx.view.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class SuccessMessage_Controller {

    @FXML
    private ImageView closeButton;

    @FXML
    private ImageView minimizeButton;

    @FXML
    private Button okButton;

    @FXML
    private Label successLabel;

    @FXML
    void closeButtonClicked(MouseEvent event) {
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    void minimizeButtonClicked(MouseEvent event) {

    }

    @FXML
    void okButtonClicked(MouseEvent event) {

        Stage stage = (Stage) okButton.getScene().getWindow();
        stage.close();


    }

    @FXML
    public void setText(String text)
    {
        successLabel.setText(text);
    }


}

