package lk.onetco.fx.view.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import lk.onetco.fx.database.DBConnection;
import lk.onetco.fx.model.Invoice;
import lk.onetco.fx.model.InvoiceDetails;

import java.math.BigDecimal;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.ResourceBundle;

public class InvoiceDetails_Controller implements Initializable {

    @FXML
    private TableView<Invoice> table_invoice;

    @FXML
    private TableColumn<Invoice, Integer> invoice_id;

    @FXML
    private TableColumn<Invoice, String> invoice_code;

    @FXML
    private TableColumn<Invoice, Integer> item_id;

    @FXML
    private TableColumn<Invoice, Integer> item_qty;

    @FXML
    private TableColumn<Invoice, BigDecimal> item_price;

    @FXML
    private TableColumn<Invoice, BigDecimal> item_discount;

    @FXML
    private TableColumn<Invoice, BigDecimal> total;





    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {
            showInvoices();
            showInvoiceDetails();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    public ObservableList<Invoice> getAllInvoices() throws SQLException {

        ObservableList<Invoice> invoices = FXCollections.observableArrayList();

        Connection connection = null;
        try {
            connection = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String Query = "SELECT * FROM invoice";
        Statement statement;
        ResultSet resultset;
        Invoice invoice;

        statement = connection.createStatement();
        resultset = statement.executeQuery(Query);

        while (resultset.next()) {
            invoice = new Invoice(resultset.getInt("invoice_id"), resultset.getString("invoice_code"), resultset.getInt("invoice_item_id"),resultset.getInt("invoice__item_qty"),resultset.getBigDecimal("invoice_item_price"),resultset.getBigDecimal("invoice_item_discount"),resultset.getBigDecimal("invoice_total_price"));
            invoices.add(invoice);


        }
        return invoices;

    }


    public void showInvoices() throws SQLException {

        ObservableList<Invoice> list = getAllInvoices();

        invoice_id.setCellValueFactory(new PropertyValueFactory<Invoice, Integer>("invoiceId"));
        invoice_code.setCellValueFactory(new PropertyValueFactory<Invoice, String>("invoiceCode"));
        item_id.setCellValueFactory(new PropertyValueFactory<Invoice, Integer>("invoiceItemId"));
        item_qty.setCellValueFactory(new PropertyValueFactory<Invoice, Integer>("invoiceItemQty"));
        item_price.setCellValueFactory(new PropertyValueFactory<Invoice, BigDecimal>("invoiceItemPrice"));
        item_discount.setCellValueFactory(new PropertyValueFactory<Invoice, BigDecimal>("invoicetemDiscount"));
        total.setCellValueFactory(new PropertyValueFactory<Invoice, BigDecimal>("invoiceTotalPrice"));

        table_invoice.setItems(list);
    }

    //Invoice Details Table

    @FXML
    private TableView<InvoiceDetails> invoice_details_table;


    @FXML
    private TableColumn<InvoiceDetails, Integer> invoice_d_id;

    @FXML
    private TableColumn<InvoiceDetails, Integer> inv_d_id;

    @FXML
    private TableColumn<InvoiceDetails, Integer> invoice_item_id;

    @FXML
    private TableColumn<InvoiceDetails, String> invoice_item_name;

    @FXML
    private TableColumn<InvoiceDetails, BigDecimal> invoice_item_price;

    @FXML
    private TableColumn<InvoiceDetails, Date> invoice_date;

    public ObservableList<InvoiceDetails> getAllInvoiceDetails() throws SQLException {

        ObservableList<InvoiceDetails> invoiceDetails = FXCollections.observableArrayList();

        Connection connection = null;
        try {
            connection = DBConnection.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String Query = "SELECT * FROM invoice_details";
        Statement statement;
        ResultSet resultset;
        InvoiceDetails invo;

        statement = connection.createStatement();
        resultset = statement.executeQuery(Query);

        while (resultset.next()) {
            invo = new InvoiceDetails(resultset.getInt("invoice_details_id"), resultset.getInt("invoice_details_invoice_id"),
                    resultset.getInt("invoice_details_item_id"),resultset.getString("invoice_details_item_name"),
                    resultset.getBigDecimal("invoice_details_item_price"),resultset.getDate("invoice_details_date"));

            invoiceDetails.add(invo);


        }
        return invoiceDetails;

    }

    public void showInvoiceDetails() throws SQLException {

        ObservableList<InvoiceDetails> list = getAllInvoiceDetails();

        invoice_d_id.setCellValueFactory(new PropertyValueFactory<InvoiceDetails, Integer>("invoiceDetailsId"));
        inv_d_id.setCellValueFactory(new PropertyValueFactory<InvoiceDetails, Integer>("invoiceId"));
        invoice_item_id.setCellValueFactory(new PropertyValueFactory<InvoiceDetails, Integer>("invoiceDetailsItemId"));
        invoice_item_name.setCellValueFactory(new PropertyValueFactory<InvoiceDetails, String>("invoiceDetailsItemName"));
        invoice_item_price.setCellValueFactory(new PropertyValueFactory<InvoiceDetails, BigDecimal>("invoiceDetailsItemPrice"));
        invoice_date.setCellValueFactory(new PropertyValueFactory<InvoiceDetails, Date>("invoiceDetailsDate"));

        invoice_details_table.setItems(list);
    }





}
