package lk.onetco.fx.view.controller;

import javafx.animation.FadeTransition;
import javafx.animation.ScaleTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import lk.onetco.fx.dao.CommonDao;
import lk.onetco.fx.dao.impl.CommonDaoImpl;
import lk.onetco.fx.dao.impl.ItemDaoImpl;
import lk.onetco.fx.main.Main;
import lk.onetco.fx.model.CommonModel;
import lk.onetco.fx.model.Item;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class Dashboard_Controller implements Initializable {


    private double xOffset = 0;
    private double yOffset = 0;

    @FXML
    private Label lblMenu;

    @FXML
    private Pane subAnchor;

    @FXML
    private Label lblMain;

    @FXML
    private ImageView imgClose;

    @FXML
    private ImageView imgMinimize;

    @FXML
    private ImageView imgInvoice;

    @FXML
    private ImageView imgReports;

    @FXML
    private ImageView imgStockManagement;

    @FXML
    private ImageView imgUserManagement;

    @FXML
    private ImageView imgUser;

    @FXML
    private ImageView imgSignOut;

    @FXML
    private AnchorPane rootPane;

    @FXML
    private ImageView categoryManagement;

    @FXML
    private Label ItemsLabel;

    @FXML
    private Label lblOrderCount;

    @FXML
    private Label lblCustomerCount;

    @FXML
    private Label lblItemsCount;

    @FXML
    private Label lblTotalIncome;

    @FXML
    private TableView<CommonModel> tblRecentInvoices;

    @FXML
    private TableColumn<CommonModel, String> colInvoiceNo;

    @FXML
    private TableColumn<CommonModel, String> colCustomerName;

    @FXML
    private TableColumn<CommonModel, String> colContactNo;

    @FXML
    private TableColumn<CommonModel, BigDecimal> colnvoiceTotal;

    final ObservableList<CommonModel> recentInvoices = FXCollections.observableArrayList();

    CommonDaoImpl commonDao =new CommonDaoImpl();
    int invoiceCount=0;
    int customerCount=0;
    int itemCount=0;
    BigDecimal totalIncome=BigDecimal.ZERO;


    @FXML
    void imgCloseMouseClicked(MouseEvent event) {
        Stage stage = (Stage) imgClose.getScene().getWindow();
        stage.close();
    }

    @FXML
    void imgMinimizeMouseClicked(MouseEvent event) {
        Stage stage = (Stage) imgClose.getScene().getWindow();
        stage.setIconified(true);
    }
    @FXML
    void playMouseEnterAnimation(MouseEvent event) {
        if (event.getSource() instanceof ImageView){
            ImageView icon = (ImageView) event.getSource();

            switch(icon.getId()){
                case "imgStockManagement":
                    lblMenu.setText("Stock Management");
                    break;
                case "imgUserManagement":
                    lblMenu.setText("User Management");
                    break;
                case "imgInvoice":
                    lblMenu.setText("Invoice Details");
                    break;
                case "imgUser":
                    lblMenu.setText("User Details");
                    break;
                case "imgReports":
                    lblMenu.setText("Reports Summary");
                    break;
                case "categoryManagement":
                    lblMenu.setText("Manage Categories");
                    break;
            }

            ScaleTransition scaleT =new ScaleTransition(Duration.millis(200), icon);
            scaleT.setToX(1.2);
            scaleT.setToY(1.2);
            scaleT.play();

            DropShadow glow = new DropShadow();
            glow.setColor(Color.SKYBLUE);
            glow.setWidth(20);
            glow.setHeight(20);
            glow.setRadius(20);
            icon.setEffect(glow);
        }
    }

    @FXML
    void playMouseExitAnimation(MouseEvent event) {
        if (event.getSource() instanceof ImageView){
            ImageView icon = (ImageView) event.getSource();
            ScaleTransition scaleT =new ScaleTransition(Duration.millis(200), icon);
            scaleT.setToX(1);
            scaleT.setToY(1);
            scaleT.play();

            icon.setEffect(null);
            lblMenu.setText("Admin Dashboard");
        }
    }

    @FXML
    void btnStockManagementMouseClicked(MouseEvent event) throws IOException {

        Pane pane = getInterface("ViewItem.fxml");
        main.setCenter(pane);
    }

    @FXML
    void addUser(MouseEvent event) throws IOException {

        Pane pane = getInterface("AddUser.fxml");
        main.setCenter(pane);

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        FadeTransition fadeIn = new FadeTransition(Duration.millis(2300), rootPane);
        fadeIn.setFromValue(0.0);
        fadeIn.setToValue(1.0);
        fadeIn.play();

        lblMenu.setText("Admin Dashboard");


        Pane pane = null;
        try {
            pane = getInterface("DashboardHome.fxml");
        } catch (IOException e) {
            e.printStackTrace();
        }
        main.setCenter(pane);

    }


    @FXML
    private BorderPane main;


    @FXML
    void invoiceClicked(MouseEvent event) throws IOException {

        Pane pane = getInterface("InvoiceDetails.fxml");
        main.setCenter(pane);

    }

    @FXML
    void categoryManagementClciked(MouseEvent event) throws IOException {

        Pane pane = getInterface("AddCategory.fxml");
        main.setCenter(pane);

    }
    @FXML
    void reportManagementClciked(MouseEvent event) throws IOException {
        Pane pane = getInterface("Reports.fxml");
        main.setCenter(pane);
    }
    private Pane getInterface(String fileName) throws IOException {
        URL fileURL = Main.class.getResource("/lk/onetco/fx/view/"+fileName);

        if(fileURL == null)
        {
            System.out.println("File Not Found");
        }

        Pane pane = new FXMLLoader().load(fileURL);

        return  pane;
    }


    @FXML
    void btnSignOut(MouseEvent event) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/lk/onetco/fx/view/Login.fxml"));
        Parent root = null;
        try {
            root = (Parent) fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.initStyle(StageStyle.UNDECORATED);
        stage.show();
        ((Node) (event.getSource())).getScene().getWindow().hide();

    }


    @FXML
    void btnUserMouseClicked(MouseEvent event) {
        Pane pane = null;
        try {
            pane = getInterface("DashboardHome.fxml");
        } catch (IOException e) {
            e.printStackTrace();
        }
        main.setCenter(pane);
    }

}


