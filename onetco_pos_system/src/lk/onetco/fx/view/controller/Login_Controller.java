package lk.onetco.fx.view.controller;

import com.jfoenix.controls.JFXButton;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import lk.onetco.fx.dao.UserDao;
import lk.onetco.fx.dao.impl.UserDaoImpl;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

public class Login_Controller implements Initializable {

    @FXML
    private JFXButton btnLogin;

    private double xOffset = 0;
    private double yOffset = 0;

    @FXML
    private TextField username;

    @FXML
    private PasswordField password;


    @FXML
    private ImageView imgClose;

    @FXML
    private ImageView imgMinimize;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        username.requestFocus();
    }
    @FXML
    void imgCloseMouseClicked(MouseEvent event) {
        Stage stage = (Stage) imgClose.getScene().getWindow();
        stage.close();
    }

    @FXML
    void imgMinimizeMouseClicked(MouseEvent event) {
        Stage stage = (Stage) imgClose.getScene().getWindow();
        stage.setIconified(true);
    }

    @FXML
    void btnLoginMouseClicked(MouseEvent event) throws IOException, SQLException {

        String name = username.getText();
        String pass = password.getText();


        UserDaoImpl userDao = new UserDaoImpl();
        boolean flag = userDao.userLogin(name, pass);


        if (!flag) {

            if (username.getText().isEmpty() || password.getText().isEmpty()) {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/lk/onetco/fx/view/ErrorMessage.fxml"));
                Parent root = (Parent) fxmlLoader.load();
                Stage stage = new Stage();
                stage.setScene(new Scene(root));
                stage.initStyle(StageStyle.UNDECORATED);
                stage.show();

                //ErrorMessage_Controller errorMessage_controller = new ErrorMessage_Controller();
                //errorMessage_controller.setErrorText("DDDDD");


                //((Node)(event.getSource())).getScene().getWindow().hide();

            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("Wrong User Name or Password");
                alert.show();
                return;
            }
        }
        if (UserDaoImpl.isFirstLogin(username.getText().trim())) {
            Alert confirmMsg = new Alert(Alert.AlertType.WARNING, "Please Change the Password!", ButtonType.OK);
            Optional<ButtonType> buttonType = confirmMsg.showAndWait();
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/lk/onetco/fx/view/PasswordUpdate.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.initStyle(StageStyle.UNDECORATED);
            stage.show();
            PasswordUpdateController.username = username.getText().trim();
        }

        if (!UserDaoImpl.isFirstLogin(username.getText().trim())) {
            try {
                if (isAdminLogin()) {
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/lk/onetco/fx/view/Dashboard.fxml"));
                    Parent root = (Parent) fxmlLoader.load();
                    Stage stage = new Stage();
                    stage.setScene(new Scene(root));
                    stage.initStyle(StageStyle.UNDECORATED);
                    stage.show();
                    ((Node) (event.getSource())).getScene().getWindow().hide();

                    root.setOnMousePressed(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            xOffset = event.getSceneX();
                            yOffset = event.getSceneY();
                        }
                    });
                    root.setOnMouseDragged(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            stage.setX(event.getScreenX() - xOffset);
                            stage.setY(event.getScreenY() - yOffset);
                        }
                    });
                }else {
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/lk/onetco/fx/view/UserDashboard.fxml"));
                    Parent root = (Parent) fxmlLoader.load();
                    Stage stage = new Stage();
                    stage.setScene(new Scene(root));
                    stage.initStyle(StageStyle.UNDECORATED);
                    stage.show();
                    ((Node) (event.getSource())).getScene().getWindow().hide();

                    root.setOnMousePressed(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            xOffset = event.getSceneX();
                            yOffset = event.getSceneY();
                        }
                    });
                    root.setOnMouseDragged(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            stage.setX(event.getScreenX() - xOffset);
                            stage.setY(event.getScreenY() - yOffset);
                        }
                    });
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isAdminLogin() throws SQLException {
        String userType = null;
        UserDaoImpl userDao1 = new UserDaoImpl();
        ResultSet rst = userDao1.getUserDetailsByUserName(username.getText().trim());
        if (rst.next()) {
            rst.first();
            userType = rst.getString("usertype");
        }
        if (userType.equals("Admin")) {
            return true;
        } else {
            return false;
        }
    }


}
