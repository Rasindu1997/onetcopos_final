package lk.onetco.fx.view.controller;

import com.jfoenix.controls.JFXButton;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.StringConverter;
import lk.onetco.fx.dao.impl.CategoryDaoImpl;
import lk.onetco.fx.dao.impl.ItemDaoImpl;
import lk.onetco.fx.model.Category;
import lk.onetco.fx.model.Item;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Observable;
import java.util.ResourceBundle;

public class AddItem_Controller implements Initializable {

    private double xOffset = 0;
    private double yOffset = 0;

    final ObservableList categories = FXCollections.observableArrayList();

    @FXML
    private ComboBox<Category> comboCategory =new ComboBox<>(categories);

    @FXML
    private JFXButton btnAddItem;

    @FXML
    private TextField txtItemName;

    @FXML
    private TextField txtRetailPrice;

    @FXML
    private TextField txtQty;

    @FXML
    private JFXButton btnViewItems;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            loadAllCategoriesToComboBox();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
public void clearAll(){
        txtItemName.setText(null);
        txtRetailPrice.setText(null);
        txtQty.setText(null);
        comboCategory.getSelectionModel().select(-1);
}

    @FXML
    void btnAddItemMouseClicked(MouseEvent event) throws SQLException {
        ItemDaoImpl itemDao = new ItemDaoImpl();
        Item item=new Item();
        item.setItemName(txtItemName.getText());
        item.setItemCategoryCode(comboCategory.getSelectionModel().getSelectedItem().getCategoryCode());
        item.setRetailPrice(new BigDecimal(txtRetailPrice.getText()));
        item.setItemStockQty(Integer.valueOf(txtQty.getText()));
        if(itemDao.addItem(item)){
            clearAll();
        }
    }


public void loadAllCategoriesToComboBox() throws SQLException {
    CategoryDaoImpl category = new CategoryDaoImpl();
    ResultSet rst = category.getAllCategory();
    while (rst.next()) {
        categories.addAll(new Category(rst.getString("category_code"),rst.getString("category_name")));
    }
    comboCategory.setMaxHeight(30);

    comboCategory.setConverter(new StringConverter<Category>() {
        @Override
        public String toString(Category object) {
            return object.getCategoryName();
        }

        @Override
        public Category fromString(String string) {
            return comboCategory.getItems().stream().filter(ap->
                    ap.getCategoryName().equals(string)).findFirst().orElse(null);
        }
    });
    comboCategory.setItems(categories);

    rst.close();
}

    @FXML
    void btnViewItemMouseClicked(MouseEvent event) {
        try{
            FXMLLoader fxmlLoader =  new FXMLLoader(getClass().getResource("/lk/onetco/fx/view/ViewItem.fxml"));
            Parent root=(Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.initStyle(StageStyle.UNDECORATED);
            stage.show();
            ((Node)(event.getSource())).getScene().getWindow().hide();

            root.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    xOffset = event.getSceneX();
                    yOffset = event.getSceneY();
                }
            });
            root.setOnMouseDragged(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    stage.setX(event.getScreenX() - xOffset);
                    stage.setY(event.getScreenY() - yOffset);
                }
            });
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
