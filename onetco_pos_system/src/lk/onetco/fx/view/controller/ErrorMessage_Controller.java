package lk.onetco.fx.view.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class ErrorMessage_Controller implements Initializable {

    @FXML
    private ImageView imgClose;

    @FXML
    private ImageView imgMinimize;

    @FXML
    private Label errorText;

    @FXML
    private Button okButton;

    @FXML
    void imgCloseMouseClicked(MouseEvent event) {

        Stage stage = (Stage) imgClose.getScene().getWindow();
        stage.close();

    }

    @FXML
    void imgMinimizeMouseClicked(MouseEvent event) {

        Stage stage = (Stage) imgClose.getScene().getWindow();
        stage.setIconified(true);

    }


    void setErrorText(String text)
    {
        errorText.setText(text);
        System.out.println("DHDJH");
    }


    @FXML
    void okButtonClicked(MouseEvent event) {

        Stage stage = (Stage) okButton.getScene().getWindow();
        stage.close();

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
