package lk.onetco.fx.validation;

import java.math.BigDecimal;
import java.sql.Date;

public class Validation {

    public static int getIntValueFromString(String value) {
        if (isIntegerValue(value)) {
            return Integer.parseInt(value);
        } else {
            return 0;
        }
    }

    public static BigDecimal getBigDecimalValueFromString(String value) {
        if (value != null) {
            value = value.replaceAll(",", "");
        }
        if (isBigDecimalValue(value)) {
            return new BigDecimal(value);
        } else {
            return BigDecimal.ZERO;
        }
    }

    public static boolean isIntegerValue(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    public static boolean isBigDecimalValue(String value) {
        try {
            if (value != null) {
                value = value.replaceAll(",", "");
            }
            new BigDecimal(value);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    public static boolean isNotEmpty(String value) {
        return (value != null && !value.trim().isEmpty());
    }

    public static Date getSqlDateByUtilDate(java.util.Date date) {
        return new Date(date.getTime());
    }

    public boolean isValidMobileNumber(String value) {
        if (!isNotEmpty(value) && value.trim().length() > 10) {
            return false;
        } else {
            return true;
        }
    }


}


