/**
 * Validator used to check whether given string is
 * no longer than the specified amount of characters.
 *
 * @author Rasindu Dilshan,Prasad Chaturanga onetco pvt LTD
 */
package lk.onetco.fx.main;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lk.onetco.fx.database.DBConnection;

import java.awt.*;
import java.sql.Connection;

public class Main extends Application {
    private double xOffset = 0;
    private double yOffset = 0;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/lk/onetco/fx/view/Login.fxml"));
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                primaryStage.setX(event.getScreenX() - xOffset);
                primaryStage.setY(event.getScreenY() - yOffset);
            }
        });

        Connection connection = DBConnection.getConnection();
        if(connection!=null){
            System.out.println("Database Connected!!");
        }
        else{
            System.out.println("Database Not Connected!!");
        }
        primaryStage.setTitle("Onetco-POS_V_1.1");
        Image icon = new Image(getClass().getResourceAsStream("/lk/onetco/fx/images/ed69987ed4.png"));
        primaryStage.getIcons().add(icon);
        Scene mainScene = new Scene(root);
        primaryStage.setScene(mainScene);
        primaryStage.setResizable(false);
        primaryStage.initStyle(StageStyle.UNDECORATED);

//        primaryStage.show();
//        primaryStage.setScene(new Scene(root, 800, 562));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);


    }
}
