package lk.onetco.fx.model;

import java.math.BigDecimal;

public class Item {
    private int itemId;
    private String itemCode;
    private String itemCategoryCode;
    private String itemName;
    private BigDecimal retailPrice;
    private int itemStockQty;
    private String itemWarranty;


    public Item() {
    }



    public Item(int id,String itemCode, String itemCategoryCode, String itemName, BigDecimal retailPrice, int itemStockQty,String warrent) {
        this.itemId=id;
        this.itemCode = itemCode;
        this.itemCategoryCode = itemCategoryCode;
        this.itemName = itemName;
        this.retailPrice = retailPrice;
        this.itemStockQty = itemStockQty;
        this.itemWarranty=warrent;
    }

    public String getItemWarranty() {
        return itemWarranty;
    }

    public void setItemWarranty(String itemWarranty) {
        this.itemWarranty = itemWarranty;
    }

    public Item(String item_code, String item_code1, String item_name, BigDecimal item_retail_price) {
    }


    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemCategoryCode() {
        return itemCategoryCode;
    }

    public void setItemCategoryCode(String itemCategoryCode) {
        this.itemCategoryCode = itemCategoryCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public BigDecimal getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(BigDecimal retailPrice) {
        this.retailPrice = retailPrice;
    }

    public int getItemStockQty() {
        return itemStockQty;
    }

    public void setItemStockQty(int itemStockQty) {
        this.itemStockQty = itemStockQty;
    }
}
