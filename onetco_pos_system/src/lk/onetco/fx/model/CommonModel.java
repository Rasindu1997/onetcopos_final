package lk.onetco.fx.model;

import java.math.BigDecimal;

public class CommonModel {
    private int invoiceId;
    private String invoiceCode;
    private String customerName;
    private String contactNo;
    private BigDecimal invoiceTotal;

    public CommonModel(String invoiceCode, String customerName, String contactNo, BigDecimal invoiceTotal) {
        this.invoiceCode = invoiceCode;
        this.customerName = customerName;
        this.contactNo = contactNo;
        this.invoiceTotal = invoiceTotal;
    }

    public CommonModel() {
    }

    public CommonModel(int invoiceId, String invoiceCode, String customerName, String contactNo, BigDecimal invoiceTotal) {
        this.invoiceId = invoiceId;
        this.invoiceCode = invoiceCode;
        this.customerName = customerName;
        this.contactNo = contactNo;
        this.invoiceTotal = invoiceTotal;
    }

    public int getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(int invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public BigDecimal getInvoiceTotal() {
        return invoiceTotal;
    }

    public void setInvoiceTotal(BigDecimal invoiceTotal) {
        this.invoiceTotal = invoiceTotal;
    }
}
