package lk.onetco.fx.model;

import java.math.BigDecimal;
import java.util.Date;

public class Quotation {

    private int quotationId;
    private String quotationCode;
    private String itemName;
    private int itemQuantity;
    private BigDecimal itemPrice;
    private Date quotationDate;

    public Quotation() {
    }

    public Quotation(int quotationId, String quotationCode, String itemName, int itemQuantity, BigDecimal itemPrice, Date quotationDate) {
        this.quotationId = quotationId;
        this.quotationCode = quotationCode;
        this.itemName = itemName;
        this.itemQuantity = itemQuantity;
        this.itemPrice = itemPrice;
        this.quotationDate = quotationDate;
    }

    public int getQuotationId() {
        return quotationId;
    }

    public void setQuotationId(int quotationId) {
        this.quotationId = quotationId;
    }

    public String getQuotationCode() {
        return quotationCode;
    }

    public void setQuotationCode(String quotationCode) {
        this.quotationCode = quotationCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getItemQuantity() {
        return itemQuantity;
    }

    public void setItemQuantity(int itemQuantity) {
        this.itemQuantity = itemQuantity;
    }

    public BigDecimal getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(BigDecimal itemPrice) {
        this.itemPrice = itemPrice;
    }

    public Date getQuotationDate() {
        return quotationDate;
    }

    public void setQuotationDate(Date quotationDate) {
        this.quotationDate = quotationDate;
    }




}
