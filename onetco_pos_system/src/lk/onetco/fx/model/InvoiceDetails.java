package lk.onetco.fx.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

public class InvoiceDetails {
private int invoiceDetailsId;

    public InvoiceDetails(int invoiceDetailsId, int invoiceId, int invoiceDetailsItemId, String invoiceDetailsItemName, BigDecimal invoiceDetailsItemPrice, Date invoiceDetailsDate) {
        this.invoiceDetailsId = invoiceDetailsId;
        this.invoiceId = invoiceId;
        this.invoiceDetailsItemId = invoiceDetailsItemId;
        this.invoiceDetailsItemName = invoiceDetailsItemName;
        this.invoiceDetailsItemPrice = invoiceDetailsItemPrice;
        this.invoiceDetailsDate = invoiceDetailsDate;
    }

    private int invoiceId;
private int invoiceDetailsItemId;
private String invoiceDetailsItemName;
private BigDecimal invoiceDetailsItemPrice;
private Date invoiceDetailsDate;
private int invoiceDetialsItemQty;
private BigDecimal invoiceDetailsItemDiscount;

    public BigDecimal getInvoiceDetailsItemDiscount() {
        return invoiceDetailsItemDiscount;
    }

    public void setInvoiceDetailsItemDiscount(BigDecimal invoiceDetailsItemDiscount) {
        this.invoiceDetailsItemDiscount = invoiceDetailsItemDiscount;
    }

    public InvoiceDetails(int invoiceDetailsId, int invoiceId, int invoiceDetailsItemId, String invoiceDetailsItemName, BigDecimal invoiceDetailsItemPrice, Date invoiceDetailsDate, int invoiceDetialsItemQty, BigDecimal invoiceDetailsItemDiscount) {
        this.invoiceDetailsId = invoiceDetailsId;
        this.invoiceId = invoiceId;
        this.invoiceDetailsItemId = invoiceDetailsItemId;
        this.invoiceDetailsItemName = invoiceDetailsItemName;
        this.invoiceDetailsItemPrice = invoiceDetailsItemPrice;
        this.invoiceDetailsDate = invoiceDetailsDate;
        this.invoiceDetialsItemQty = invoiceDetialsItemQty;
        this.invoiceDetailsItemDiscount = invoiceDetailsItemDiscount;
    }

    public InvoiceDetails() {
    }



    public int getInvoiceDetialsItemQty() {
        return invoiceDetialsItemQty;
    }

    public void setInvoiceDetialsItemQty(int invoiceDetialsItemQty) {
        this.invoiceDetialsItemQty = invoiceDetialsItemQty;
    }

    public int getInvoiceDetailsId() {
        return invoiceDetailsId;
    }

    public void setInvoiceDetailsId(int invoiceDetailsId) {
        this.invoiceDetailsId = invoiceDetailsId;
    }

    public int getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(int invoiceId) {
        this.invoiceId = invoiceId;
    }

    public int getInvoiceDetailsItemId() {
        return invoiceDetailsItemId;
    }

    public void setInvoiceDetailsItemId(int invoiceDetailsItemCode) {
        this.invoiceDetailsItemId = invoiceDetailsItemCode;
    }

    public String getInvoiceDetailsItemName() {
        return invoiceDetailsItemName;
    }

    public void setInvoiceDetailsItemName(String invoiceDetailsItemName) {
        this.invoiceDetailsItemName = invoiceDetailsItemName;
    }

    public BigDecimal getInvoiceDetailsItemPrice() {
        return invoiceDetailsItemPrice;
    }

    public void setInvoiceDetailsItemPrice(BigDecimal invoiceDetailsItemPrice) {
        this.invoiceDetailsItemPrice = invoiceDetailsItemPrice;
    }
    public Date getInvoiceDetailsDate() {
        return invoiceDetailsDate;
    }

    public void setInvoiceDetailsDate(Date invoiceDetailsDate) {
        this.invoiceDetailsDate = invoiceDetailsDate;
    }


}
