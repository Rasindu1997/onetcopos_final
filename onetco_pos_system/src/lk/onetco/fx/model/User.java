package lk.onetco.fx.model;

public class User {

    private int userid;
    private String username;
    private String password;
    private  String userType;
    private int userPasswordStatus;

    public User(int userid, String username, String password, String userType) {
        this.userid = userid;
        this.username = username;
        this.password = password;
        this.userType = userType;
    }

    public static  final int notLogged=0;
    public static  final int logged=1;

    public User(int userid, String username, String password, String userType,int passwrdstatus) {
        this.userid = userid;
        this.username = username;
        this.password = password;
        this.userType = userType;
        this.userPasswordStatus=passwrdstatus;
    }

    public User(){}

    public int getUserPasswordStatus() {
        return userPasswordStatus;
    }

    public void setUserPasswordStatus(int userPasswordStatus) {
        this.userPasswordStatus = userPasswordStatus;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}
