package lk.onetco.fx.model;

import java.math.BigDecimal;

public class Payment {
    private int paymentId;
    private int paymentInvoiceId;
    private int paymentCustomerId;
    private BigDecimal paymentTotal;
    private BigDecimal paymentDiscountedPrice;
    private BigDecimal paymentBalance;
    private String paymentRemark;

    public Payment() {
    }

    public int getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    public int getPaymentInvoiceId() {
        return paymentInvoiceId;
    }

    public void setPaymentInvoiceId(int paymentInvoiceId) {
        this.paymentInvoiceId = paymentInvoiceId;
    }

    public int getPaymentCustomerId() {
        return paymentCustomerId;
    }

    public void setPaymentCustomerId(int paymentCustomerId) {
        this.paymentCustomerId = paymentCustomerId;
    }

    public BigDecimal getPaymentTotal() {
        return paymentTotal;
    }

    public void setPaymentTotal(BigDecimal paymentTotal) {
        this.paymentTotal = paymentTotal;
    }

    public BigDecimal getPaymentDiscountedPrice() {
        return paymentDiscountedPrice;
    }

    public void setPaymentDiscountedPrice(BigDecimal paymentDiscountedPrice) {
        this.paymentDiscountedPrice = paymentDiscountedPrice;
    }

    public BigDecimal getPaymentBalance() {
        return paymentBalance;
    }

    public void setPaymentBalance(BigDecimal paymentBalance) {
        this.paymentBalance = paymentBalance;
    }

    public String getPaymentRemark() {
        return paymentRemark;
    }

    public void setPaymentRemark(String paymentRemark) {
        this.paymentRemark = paymentRemark;
    }
}
