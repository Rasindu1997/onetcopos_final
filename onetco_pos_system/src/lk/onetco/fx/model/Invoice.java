package lk.onetco.fx.model;

import java.math.BigDecimal;
import java.util.Date;

public class Invoice {
    private int invoiceId;
    private String invoiceCode;
    private int invoiceItemId;
    private int invoiceItemQty;
    private BigDecimal invoiceItemPrice;
    private BigDecimal invoicetemDiscount;
    private BigDecimal invoiceTotalPrice;
    private Date invoiceDate;

    public Invoice(int invoiceId, String invoiceCode, int invoiceItemId, int invoiceItemQty, BigDecimal invoiceItemPrice, BigDecimal invoicetemDiscount, BigDecimal invoiceTotalPrice) {
        this.invoiceId = invoiceId;
        this.invoiceCode = invoiceCode;
        this.invoiceItemId = invoiceItemId;
        this.invoiceItemQty = invoiceItemQty;
        this.invoiceItemPrice = invoiceItemPrice;
        this.invoicetemDiscount = invoicetemDiscount;
        this.invoiceTotalPrice = invoiceTotalPrice;
    }

    public Invoice(int invoiceId, String invoiceCode, Date invoiceDate) {
        this.invoiceId = invoiceId;
        this.invoiceCode = invoiceCode;
        this.invoiceDate = invoiceDate;
    }

    public Invoice() {
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public int getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(int invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public int getInvoiceItemId() {
        return invoiceItemId;
    }

    public void setInvoiceItemId(int invoiceItemId) {
        this.invoiceItemId = invoiceItemId;
    }

    public int getInvoiceItemQty() {
        return invoiceItemQty;
    }

    public void setInvoiceItemQty(int invoiceItemQty) {
        this.invoiceItemQty = invoiceItemQty;
    }

    public BigDecimal getInvoiceItemPrice() {
        return invoiceItemPrice;
    }

    public void setInvoiceItemPrice(BigDecimal invoiceItemPrice) {
        this.invoiceItemPrice = invoiceItemPrice;
    }

    public BigDecimal getInvoicetemDiscount() {
        return invoicetemDiscount;
    }

    public void setInvoicetemDiscount(BigDecimal invoicetemDiscount) {
        this.invoicetemDiscount = invoicetemDiscount;
    }

    public BigDecimal getInvoiceTotalPrice() {
        return invoiceTotalPrice;
    }

    public void setInvoiceTotalPrice(BigDecimal invoiceTotalPrice) {
        this.invoiceTotalPrice = invoiceTotalPrice;
    }
}
