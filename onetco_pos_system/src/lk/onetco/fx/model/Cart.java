package lk.onetco.fx.model;

import java.math.BigDecimal;

public class Cart {
    private int itemId;
    private String itemCode;
    private String itemCategoryCode;
    private String itemName;
    private BigDecimal retailPrice;
    private int Qty;
    private BigDecimal discount;
    private BigDecimal total;

    public Cart() {
    }

    public Cart(int itemId, String itemCode, String itemCategoryCode, String itemName, BigDecimal retailPrice, int qty, BigDecimal discount, BigDecimal total) {
        this.itemId = itemId;
        this.itemCode = itemCode;
        this.itemCategoryCode = itemCategoryCode;
        this.itemName = itemName;
        this.retailPrice = retailPrice;
        Qty = qty;
        this.discount = discount;
        this.total = total;
    }

    public Cart(int itemId,String itemName, BigDecimal retailPrice, int qty, BigDecimal discount, BigDecimal total) {
        this.itemId = itemId;
        this.itemName = itemName;
        this.retailPrice = retailPrice;
        Qty = qty;
        this.discount = discount;
        this.total = total;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemCategoryCode() {
        return itemCategoryCode;
    }

    public void setItemCategoryCode(String itemCategoryCode) {
        this.itemCategoryCode = itemCategoryCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public BigDecimal getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(BigDecimal retailPrice) {
        this.retailPrice = retailPrice;
    }

    public int getQty() {
        return Qty;
    }

    public void setQty(int qty) {
        Qty = qty;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }


}
