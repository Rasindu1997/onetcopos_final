-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.7.29-log


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema nowlit_lighting
--

CREATE DATABASE IF NOT EXISTS nowlit_lighting;
USE nowlit_lighting;

--
-- Definition of table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `category_id` int(10) NOT NULL AUTO_INCREMENT,
  `category_code` varchar(15) DEFAULT NULL,
  `category_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` (`category_id`,`category_code`,`category_name`) VALUES 
 (1,'0000001','Moving Head Light Series'),
 (2,'0000002','LED Moving Head Series'),
 (3,'0000003','LED Pair Light Series'),
 (4,'0000004','new'),
 (5,'0000005','wawwd'),
 (6,'0000006','rad');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;


--
-- Definition of table `customer`
--

DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `customer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_firstname` varchar(45) NOT NULL,
  `customer_lastname` varchar(45) NOT NULL,
  `customer_contactno` varchar(12) NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer`
--

/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` (`customer_id`,`customer_firstname`,`customer_lastname`,`customer_contactno`) VALUES 
 (42,'rwad','wad','123123'),
 (43,'sadasd','dad','123123123');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;


--
-- Definition of table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
CREATE TABLE `invoice` (
  `invoice_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_code` varchar(20) NOT NULL,
  `invoice_item_id` int(10) unsigned NOT NULL,
  `invoice__item_qty` int(10) unsigned NOT NULL,
  `invoice_item_price` decimal(10,2) NOT NULL,
  `invoice_item_discount` decimal(10,2) NOT NULL,
  `invoice_total_price` decimal(10,2) NOT NULL,
  PRIMARY KEY (`invoice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice`
--

/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
INSERT INTO `invoice` (`invoice_id`,`invoice_code`,`invoice_item_id`,`invoice__item_qty`,`invoice_item_price`,`invoice_item_discount`,`invoice_total_price`) VALUES 
 (48,'INV_1',13,6,'22.00','10.00','122.90'),
 (49,'INV_49',10,3,'1230.00','10.00','3321.00');
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;


--
-- Definition of table `invoice_details`
--

DROP TABLE IF EXISTS `invoice_details`;
CREATE TABLE `invoice_details` (
  `invoice_details_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_details_invoice_id` int(10) unsigned NOT NULL,
  `invoice_details_item_id` int(10) NOT NULL,
  `invoice_details_item_name` varchar(65) NOT NULL,
  `invoice_details_item_price` decimal(10,2) NOT NULL,
  `invoice_details_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `invoice_details_item_qty` int(10) unsigned NOT NULL,
  `invoice_details_item_discount` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`invoice_details_id`)
) ENGINE=InnoDB AUTO_INCREMENT=583 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice_details`
--

/*!40000 ALTER TABLE `invoice_details` DISABLE KEYS */;
INSERT INTO `invoice_details` (`invoice_details_id`,`invoice_details_invoice_id`,`invoice_details_item_id`,`invoice_details_item_name`,`invoice_details_item_price`,`invoice_details_date`,`invoice_details_item_qty`,`invoice_details_item_discount`) VALUES 
 (580,48,12,'rq','23.00','2020-08-06 00:00:41',2,'5.00'),
 (581,48,13,'rasdas','22.00','2020-08-06 00:00:42',4,'10.00'),
 (582,49,10,'asd','1230.00','2020-08-06 00:01:27',3,'10.00');
/*!40000 ALTER TABLE `invoice_details` ENABLE KEYS */;


--
-- Definition of table `item`
--

DROP TABLE IF EXISTS `item`;
CREATE TABLE `item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_code` varchar(15) NOT NULL,
  `item_category_id` varchar(15) NOT NULL,
  `item_name` varchar(55) NOT NULL,
  `item_retail_price` decimal(10,2) NOT NULL,
  `item_stock_qty` varchar(45) NOT NULL,
  `item_warranty` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `item`
--

/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` (`item_id`,`item_code`,`item_category_id`,`item_name`,`item_retail_price`,`item_stock_qty`,`item_warranty`) VALUES 
 (1,'ITM_0','2','new','500.00','390',NULL),
 (4,'ITM_1','3','rqqw','1500.00','6',NULL),
 (5,'ITM_4','0000005','new12','1200.00','299',NULL),
 (7,'ITM_5','0000006','neew item','200.00','7',NULL),
 (8,'ITM_7','0000002','uuu','204.00','6',NULL),
 (10,'ITM_8','0000001','asd','1230.00','-4',NULL),
 (11,'ITM_10','0000001','rrraw','22.00','2',NULL),
 (12,'ITM_11','0000001','rq','23.00','0',NULL),
 (13,'ITM_12','0000003','rasdas','22.00','-1',NULL),
 (14,'ITM_13','0000002','taweaw','200.00','23',NULL);
/*!40000 ALTER TABLE `item` ENABLE KEYS */;


--
-- Definition of table `payment`
--

DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment` (
  `payment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `payment_invoice_id` int(10) unsigned NOT NULL,
  `payment_customer_id` int(10) unsigned NOT NULL,
  `payment_total` decimal(10,2) NOT NULL,
  `payment_discounted_price` decimal(10,2) NOT NULL,
  `payment_balance` decimal(10,2) NOT NULL,
  `payment_remark` varchar(265) NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment`
--

/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
INSERT INTO `payment` (`payment_id`,`payment_invoice_id`,`payment_customer_id`,`payment_total`,`payment_discounted_price`,`payment_balance`,`payment_remark`) VALUES 
 (42,48,42,'122.00','122.90','-0.90','wadasdasd'),
 (43,49,43,'4000.00','3321.00','679.00','asdad');
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;


--
-- Definition of table `quotation`
--

DROP TABLE IF EXISTS `quotation`;
CREATE TABLE `quotation` (
  `quotation_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quotation_code` varchar(45) NOT NULL,
  `quotation_item_name` varchar(45) NOT NULL,
  `quotation_item_qty` int(10) unsigned NOT NULL,
  `quotation_item_price` decimal(10,2) NOT NULL,
  `quotation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`quotation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=353 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quotation`
--

/*!40000 ALTER TABLE `quotation` DISABLE KEYS */;
/*!40000 ALTER TABLE `quotation` ENABLE KEYS */;


--
-- Definition of table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(25) NOT NULL,
  `usertype` varchar(15) NOT NULL,
  `user_password_status` int(2) unsigned NOT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`userid`,`username`,`password`,`usertype`,`user_password_status`) VALUES 
 (4,'adeee','1233','Admin',0),
 (5,'admin','12344','Admin',1),
 (6,'adeee5','4212','Admin',0),
 (7,'tasda','12333','Admin',1),
 (8,'wet','user123','User',1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
