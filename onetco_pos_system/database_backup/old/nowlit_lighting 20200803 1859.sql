-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.7.29-log


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema nowlit_lighting
--

CREATE DATABASE IF NOT EXISTS nowlit_lighting;
USE nowlit_lighting;

--
-- Definition of table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `category_id` int(10) NOT NULL AUTO_INCREMENT,
  `category_code` varchar(15) DEFAULT NULL,
  `category_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` (`category_id`,`category_code`,`category_name`) VALUES 
 (1,'0000001','Moving Head Light Series'),
 (2,'0000002','LED Moving Head Series'),
 (3,'0000003','LED Pair Light Series'),
 (4,'0000004','new'),
 (5,'0000005','wawwd'),
 (6,'0000006','rad');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;


--
-- Definition of table `customer`
--

DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `customer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_firstname` varchar(45) NOT NULL,
  `customer_lastname` varchar(45) NOT NULL,
  `customer_contactno` varchar(12) NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer`
--

/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` (`customer_id`,`customer_firstname`,`customer_lastname`,`customer_contactno`) VALUES 
 (3,'test','asfss','08123123'),
 (4,'tesaw2','tsafewe','07812333'),
 (5,'twwwwa','rtas','08123412'),
 (6,'tryrtdrd','drderdre','5756566565'),
 (7,'faaef','sdsdsd','01312312'),
 (8,'new one','asdasd','091232312'),
 (9,'new one','asdasd','091232312'),
 (10,'ffffawe','ffawe','123121231231'),
 (11,'fhregr',' dfrgr','2342432342'),
 (12,'asddd','rawr','12344'),
 (13,'fasdad','dawadasd','12412421442'),
 (14,'afsddasdsa','awdasadsad','121323123'),
 (15,'ras','wra','12344'),
 (16,'rasdasdawa','eefrfr','01211232131'),
 (17,'wrrer','feffrf','12323'),
 (18,'wrrer','feffrf','12323');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;


--
-- Definition of table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
CREATE TABLE `invoice` (
  `invoice_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_code` varchar(20) NOT NULL,
  `invoice_item_id` int(10) unsigned NOT NULL,
  `invoice__item_qty` int(10) unsigned NOT NULL,
  `invoice_item_price` decimal(10,2) NOT NULL,
  `invoice_item_discount` decimal(10,2) NOT NULL,
  `invoice_total_price` decimal(10,2) NOT NULL,
  PRIMARY KEY (`invoice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice`
--

/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
INSERT INTO `invoice` (`invoice_id`,`invoice_code`,`invoice_item_id`,`invoice__item_qty`,`invoice_item_price`,`invoice_item_discount`,`invoice_total_price`) VALUES 
 (8,'INV_1',7,6,'200.00','10.00','2880.00'),
 (9,'INV_9',4,5,'1200.00','20.00','3780.00'),
 (10,'INV_10',7,6,'200.00','5.00','2920.00'),
 (11,'INV_11',4,7,'1200.00','10.00','3060.00'),
 (12,'INV_12',1,9,'500.00','10.00','4500.00'),
 (13,'INV_13',1,5,'500.00','10.00','3510.00'),
 (14,'INV_14',1,5,'500.00','10.00','3510.00'),
 (15,'INV_15',7,6,'200.00','5.00','2920.00'),
 (16,'INV_16',7,11,'200.00','5.00','9210.00'),
 (17,'INV_17',7,11,'200.00','5.00','9690.00'),
 (18,'INV_18',4,11,'1200.00','5.00','9660.00'),
 (19,'INV_19',4,11,'1200.00','20.00','8250.00'),
 (20,'INV_20',1,2,'500.00','5.00','950.00'),
 (21,'INV_21',1,2,'500.00','10.00','900.00'),
 (22,'INV_22',7,2,'200.00','10.00','360.00'),
 (23,'INV_23',7,2,'200.00','10.00','360.00');
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;


--
-- Definition of table `invoice_details`
--

DROP TABLE IF EXISTS `invoice_details`;
CREATE TABLE `invoice_details` (
  `invoice_details_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_details_invoice_id` int(10) unsigned NOT NULL,
  `invoice_details_item_id` int(10) NOT NULL,
  `invoice_details_item_name` varchar(65) NOT NULL,
  `invoice_details_item_price` decimal(10,2) NOT NULL,
  `invoice_details_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `invoice_details_item_qty` int(10) unsigned NOT NULL,
  PRIMARY KEY (`invoice_details_id`)
) ENGINE=InnoDB AUTO_INCREMENT=547 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice_details`
--

/*!40000 ALTER TABLE `invoice_details` DISABLE KEYS */;
INSERT INTO `invoice_details` (`invoice_details_id`,`invoice_details_invoice_id`,`invoice_details_item_id`,`invoice_details_item_name`,`invoice_details_item_price`,`invoice_details_date`,`invoice_details_item_qty`) VALUES 
 (515,1,0,'rqqw','1200.00','2020-07-23 00:14:22',0),
 (516,1,0,'neew item','200.00','2020-07-23 00:14:23',0),
 (517,9,1,'new','500.00','2020-07-23 00:24:11',0),
 (518,9,4,'rqqw','1200.00','2020-07-23 00:24:11',0),
 (519,10,5,'new12','1200.00','2020-07-23 00:43:46',0),
 (520,10,7,'neew item','200.00','2020-07-23 00:43:46',0),
 (521,11,7,'neew item','200.00','2020-07-23 01:17:39',0),
 (522,11,4,'rqqw','1200.00','2020-07-23 01:17:39',0),
 (523,12,4,'rqqw','1200.00','2020-07-23 10:29:13',0),
 (524,12,1,'new','500.00','2020-07-23 11:31:10',0),
 (525,12,5,'new12','1200.00','2020-07-23 11:31:10',0),
 (526,12,4,'rqqw','1200.00','2020-07-23 11:44:43',0),
 (527,12,7,'neew item','200.00','2020-07-23 11:44:43',0),
 (528,12,1,'new','500.00','2020-07-23 11:44:43',0),
 (529,13,5,'new12','1200.00','2020-07-23 11:48:51',0),
 (530,13,1,'new','500.00','2020-07-23 11:48:51',0),
 (531,14,5,'new12','1200.00','2020-07-23 11:49:09',0),
 (532,14,1,'new','500.00','2020-07-23 11:49:09',0),
 (533,15,4,'rqqw','1200.00','2020-07-23 16:40:15',2),
 (534,15,7,'neew item','200.00','2020-07-23 16:40:15',4),
 (535,16,4,'rqqw','1200.00','2020-07-23 16:43:39',8),
 (536,16,7,'neew item','200.00','2020-07-23 16:43:39',3),
 (537,17,4,'rqqw','1200.00','2020-07-23 16:49:42',8),
 (538,17,7,'neew item','200.00','2020-07-23 16:49:42',3),
 (539,18,7,'neew item','200.00','2020-07-23 16:54:07',3),
 (540,18,4,'rqqw','1200.00','2020-07-23 16:54:08',8),
 (541,19,7,'neew item','200.00','2020-07-23 16:55:24',3),
 (542,19,4,'rqqw','1200.00','2020-07-23 16:55:24',8),
 (543,20,1,'new','500.00','2020-08-01 11:58:03',2),
 (544,21,1,'new','500.00','2020-08-03 12:56:26',2),
 (545,22,7,'neew item','200.00','2020-08-03 13:08:25',2),
 (546,23,7,'neew item','200.00','2020-08-03 13:08:46',2);
/*!40000 ALTER TABLE `invoice_details` ENABLE KEYS */;


--
-- Definition of table `item`
--

DROP TABLE IF EXISTS `item`;
CREATE TABLE `item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_code` varchar(15) NOT NULL,
  `item_category_id` varchar(15) NOT NULL,
  `item_name` varchar(55) NOT NULL,
  `item_retail_price` decimal(10,2) NOT NULL,
  `item_stock_qty` varchar(45) NOT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `item`
--

/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` (`item_id`,`item_code`,`item_category_id`,`item_name`,`item_retail_price`,`item_stock_qty`) VALUES 
 (1,'ITM_0','2','new','500.00','396'),
 (4,'ITM_1','3','rqqw','1500.00','20'),
 (5,'ITM_4','0000005','new12','1200.00','322'),
 (7,'ITM_5','0000006','neew item','200.00','16'),
 (8,'ITM_7','0000002','uuu','204.00','2'),
 (10,'ITM_8','0000001','asd','1230.00','2'),
 (11,'ITM_10','0000001','rrraw','22.00','2'),
 (12,'ITM_11','0000001','rq','23.00','2'),
 (13,'ITM_12','0000003','rasdas','22.00','3');
/*!40000 ALTER TABLE `item` ENABLE KEYS */;


--
-- Definition of table `payment`
--

DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment` (
  `payment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `payment_invoice_id` int(10) unsigned NOT NULL,
  `payment_customer_id` int(10) unsigned NOT NULL,
  `payment_total` decimal(10,2) NOT NULL,
  `payment_discounted_price` decimal(10,2) NOT NULL,
  `payment_balance` decimal(10,2) NOT NULL,
  `payment_remark` varchar(265) NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment`
--

/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
INSERT INTO `payment` (`payment_id`,`payment_invoice_id`,`payment_customer_id`,`payment_total`,`payment_discounted_price`,`payment_balance`,`payment_remark`) VALUES 
 (3,2,1,'3000.00','20.00','120.00','tses remarkkkkkkkkkkkkkkkkkkkkklllkkkkkkkkkkk'),
 (4,2,1,'4000.00','30.00','220.00','aweaaaaaaaaaaaaaaaaaaaaaaaaaaaw'),
 (5,10,5,'3000.00','15.00','80.00','aetewafefsfwefszdcscsdv dfdf'),
 (6,11,6,'3400.00','20.00','340.00','kjkkjlkkkkkkkkkkkkkkkkkkkkkkkkkkkk uikhn k,jllllllllllllllljihiuhu'),
 (7,12,7,'5000.00','30.00','500.00','racdvsdvsdsdvsdv dsfsdfdfsdfdffff'),
 (8,13,8,'3600.00','20.00','90.00','goodadasdasddw'),
 (9,14,9,'3600.00','20.00','90.00','goodadasdasddw'),
 (10,15,10,'3000.00','15.00','80.00','awdawdawwd wadd  qdwdwafad w addafffexc wef'),
 (11,16,11,'9600.00','15.00','390.00','sefsg rgergergsg rgrgrgg'),
 (12,17,12,'10000.00','10.00','310.00','2asdafdefa'),
 (13,18,13,'10000.00','15.00','340.00','aasda sdasdasdf afsddfsfsdxcxcvxvfd efwe f'),
 (14,19,14,'9000.00','25.00','750.00','sdfsdfsdfsfdsdf wefwefsdf'),
 (15,20,15,'1000.00','5.00','50.00',''),
 (16,21,16,'1000.00','10.00','100.00','dwesdfdfv d fgrgregrdvdfvdfvdfvdfb ttgtgttyt'),
 (17,22,17,'400.00','10.00','40.00','dfddfdfgf'),
 (18,23,18,'400.00','10.00','40.00','dfddfdfgf');
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;


--
-- Definition of table `quotation`
--

DROP TABLE IF EXISTS `quotation`;
CREATE TABLE `quotation` (
  `quotation_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quotation_code` varchar(45) NOT NULL,
  `quotation_item_name` varchar(45) NOT NULL,
  `quotation_item_qty` int(10) unsigned NOT NULL,
  `quotation_item_price` decimal(10,2) NOT NULL,
  `quotation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`quotation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=353 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quotation`
--

/*!40000 ALTER TABLE `quotation` DISABLE KEYS */;
INSERT INTO `quotation` (`quotation_id`,`quotation_code`,`quotation_item_name`,`quotation_item_qty`,`quotation_item_price`,`quotation_date`) VALUES 
 (308,'QUO_1','rqqw',2,'1200.00','2020-07-24 19:06:10'),
 (309,'QUO_309','neew item',3,'200.00','2020-07-24 19:06:10'),
 (310,'QUO_310','new',2,'500.00','2020-07-24 19:12:56'),
 (311,'QUO_310','neew item',5,'200.00','2020-07-24 19:12:57'),
 (312,'QUO_312','new12',2,'1200.00','2020-07-24 19:23:31'),
 (313,'QUO_312','newr1',3,'204.00','2020-07-24 19:23:31'),
 (314,'QUO_314','neew item',2,'200.00','2020-07-24 19:33:09'),
 (315,'QUO_314','newr1',4,'204.00','2020-07-24 19:33:09'),
 (316,'QUO_316','newr1',2,'204.00','2020-07-24 19:34:08'),
 (317,'QUO_316','rqqw',4,'1200.00','2020-07-24 19:34:08'),
 (318,'QUO_318','rqqw',2,'1200.00','2020-07-24 19:36:29'),
 (319,'QUO_318','newr1',4,'204.00','2020-07-24 19:36:30'),
 (320,'QUO_320','rqqw',2,'1200.00','2020-07-24 23:38:42'),
 (321,'QUO_320','neew item',3,'200.00','2020-07-24 23:38:42'),
 (322,'QUO_322','neew item',2,'200.00','2020-07-24 23:46:56'),
 (323,'QUO_322','new',3,'500.00','2020-07-24 23:46:56'),
 (324,'QUO_324','newr1',2,'204.00','2020-07-24 23:49:15'),
 (325,'QUO_324','new12',4,'1200.00','2020-07-24 23:49:15'),
 (326,'QUO_326','new12',2,'1200.00','2020-07-24 23:50:37'),
 (327,'QUO_326','newr1',2,'204.00','2020-07-24 23:50:37'),
 (328,'QUO_328','neew item',2,'200.00','2020-07-24 23:52:22'),
 (329,'QUO_328','asd',4,'12.00','2020-07-24 23:52:23'),
 (330,'QUO_330','new12',1,'1200.00','2020-07-24 23:54:37'),
 (331,'QUO_330','newr1',3,'204.00','2020-07-24 23:54:37'),
 (332,'QUO_332','rqqw',2,'1200.00','2020-07-24 23:55:45'),
 (333,'QUO_332','newr1',2,'204.00','2020-07-24 23:55:45'),
 (334,'QUO_334','new',1,'500.00','2020-07-31 11:01:55'),
 (335,'QUO_334','newr1',3,'204.00','2020-07-31 11:01:56'),
 (336,'QUO_336','new',2,'500.00','2020-08-01 10:54:41'),
 (337,'QUO_336','neew item',2,'200.00','2020-08-01 10:54:41'),
 (338,'QUO_338','new12',2,'1200.00','2020-08-01 11:01:12'),
 (339,'QUO_338','neew item',4,'200.00','2020-08-01 11:01:12'),
 (340,'QUO_340','uuu',1,'204.00','2020-08-01 11:03:24'),
 (341,'QUO_341','neew item',3,'200.00','2020-08-01 11:04:33'),
 (342,'QUO_342','asd',3,'1230.00','2020-08-01 11:14:43'),
 (343,'QUO_343','uuu',3,'204.00','2020-08-01 11:18:44'),
 (344,'QUO_343','neew item',2,'200.00','2020-08-01 11:18:44'),
 (345,'QUO_345','new12',2,'1200.00','2020-08-01 11:21:18'),
 (346,'QUO_345','neew item',3,'200.00','2020-08-01 11:21:19'),
 (347,'QUO_347','new12',2,'1200.00','2020-08-01 11:22:14'),
 (348,'QUO_348','neew item',2,'200.00','2020-08-01 11:23:35'),
 (349,'QUO_349','new12',2,'1200.00','2020-08-01 11:24:36'),
 (350,'QUO_350','new',2,'500.00','2020-08-01 11:25:33'),
 (351,'QUO_351','asd',2,'1230.00','2020-08-01 11:34:09'),
 (352,'QUO_351','rq',3,'23.00','2020-08-01 11:34:09');
/*!40000 ALTER TABLE `quotation` ENABLE KEYS */;


--
-- Definition of table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(25) NOT NULL,
  `usertype` varchar(15) NOT NULL,
  `user_password_status` int(2) unsigned NOT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`userid`,`username`,`password`,`usertype`,`user_password_status`) VALUES 
 (4,'adeee','1233','Admin',0),
 (5,'admin','12344','Admin',1),
 (6,'adeee5','4212','Admin',0),
 (7,'tasda','12333','Admin',1),
 (8,'wet','user123','User',1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
