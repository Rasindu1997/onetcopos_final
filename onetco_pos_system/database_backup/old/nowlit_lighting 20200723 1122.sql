-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.7.29-log


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema nowlit_lighting
--

CREATE DATABASE IF NOT EXISTS nowlit_lighting;
USE nowlit_lighting;

--
-- Definition of table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `category_id` int(10) NOT NULL AUTO_INCREMENT,
  `category_code` varchar(15) DEFAULT NULL,
  `category_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` (`category_id`,`category_code`,`category_name`) VALUES 
 (1,'0000001','Moving Head Light Series'),
 (2,'0000002','LED Moving Head Series'),
 (3,'0000003','LED Pair Light Series'),
 (4,'0000004','new');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;


--
-- Definition of table `customer`
--

DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `customer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_firstname` varchar(45) NOT NULL,
  `customer_lastname` varchar(45) NOT NULL,
  `customer_contactno` varchar(12) NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer`
--

/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` (`customer_id`,`customer_firstname`,`customer_lastname`,`customer_contactno`) VALUES 
 (3,'test','asfss','08123123'),
 (4,'tesaw2','tsafewe','07812333'),
 (5,'twwwwa','rtas','08123412'),
 (6,'tryrtdrd','drderdre','5756566565');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;


--
-- Definition of table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
CREATE TABLE `invoice` (
  `invoice_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_code` varchar(20) NOT NULL,
  `invoice_item_id` int(10) unsigned NOT NULL,
  `invoice__item_qty` int(10) unsigned NOT NULL,
  `invoice_item_price` decimal(10,2) NOT NULL,
  `invoice_item_discount` decimal(10,2) NOT NULL,
  `invoice_total_price` decimal(10,2) NOT NULL,
  PRIMARY KEY (`invoice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice`
--

/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
INSERT INTO `invoice` (`invoice_id`,`invoice_code`,`invoice_item_id`,`invoice__item_qty`,`invoice_item_price`,`invoice_item_discount`,`invoice_total_price`) VALUES 
 (8,'INV_1',7,6,'200.00','10.00','2880.00'),
 (9,'INV_9',4,5,'1200.00','20.00','3780.00'),
 (10,'INV_10',7,6,'200.00','5.00','2920.00'),
 (11,'INV_11',4,7,'1200.00','10.00','3060.00');
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;


--
-- Definition of table `invoice_details`
--

DROP TABLE IF EXISTS `invoice_details`;
CREATE TABLE `invoice_details` (
  `invoice_details_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_details_invoice_id` int(10) unsigned NOT NULL,
  `invoice_details_item_id` int(10) NOT NULL,
  `invoice_details_item_name` varchar(65) NOT NULL,
  `invoice_details_item_price` decimal(10,2) NOT NULL,
  `invoice_details_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`invoice_details_id`)
) ENGINE=InnoDB AUTO_INCREMENT=524 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice_details`
--

/*!40000 ALTER TABLE `invoice_details` DISABLE KEYS */;
INSERT INTO `invoice_details` (`invoice_details_id`,`invoice_details_invoice_id`,`invoice_details_item_id`,`invoice_details_item_name`,`invoice_details_item_price`,`invoice_details_date`) VALUES 
 (515,1,0,'rqqw','1200.00','2020-07-23 00:14:22'),
 (516,1,0,'neew item','200.00','2020-07-23 00:14:23'),
 (517,9,1,'new','500.00','2020-07-23 00:24:11'),
 (518,9,4,'rqqw','1200.00','2020-07-23 00:24:11'),
 (519,10,5,'new12','1200.00','2020-07-23 00:43:46'),
 (520,10,7,'neew item','200.00','2020-07-23 00:43:46'),
 (521,11,7,'neew item','200.00','2020-07-23 01:17:39'),
 (522,11,4,'rqqw','1200.00','2020-07-23 01:17:39'),
 (523,12,4,'rqqw','1200.00','2020-07-23 10:29:13');
/*!40000 ALTER TABLE `invoice_details` ENABLE KEYS */;


--
-- Definition of table `item`
--

DROP TABLE IF EXISTS `item`;
CREATE TABLE `item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_code` varchar(15) NOT NULL,
  `item_category_id` varchar(15) NOT NULL,
  `item_name` varchar(55) NOT NULL,
  `item_retail_price` decimal(10,2) NOT NULL,
  `item_stock_qty` varchar(45) NOT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `item`
--

/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` (`item_id`,`item_code`,`item_category_id`,`item_name`,`item_retail_price`,`item_stock_qty`) VALUES 
 (1,'ITM_0','2','new','500.00','400'),
 (4,'ITM_1','3','rqqw','1200.00','32'),
 (5,'ITM_4','3','new12','1200.00','322'),
 (7,'ITM_5','0000004','neew item','200.00','23');
/*!40000 ALTER TABLE `item` ENABLE KEYS */;


--
-- Definition of table `payment`
--

DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment` (
  `payment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `payment_invoice_id` int(10) unsigned NOT NULL,
  `payment_customer_id` int(10) unsigned NOT NULL,
  `payment_total` decimal(10,2) NOT NULL,
  `payment_discounted_price` decimal(10,2) NOT NULL,
  `payment_balance` decimal(10,2) NOT NULL,
  `payment_remark` varchar(265) NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment`
--

/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
INSERT INTO `payment` (`payment_id`,`payment_invoice_id`,`payment_customer_id`,`payment_total`,`payment_discounted_price`,`payment_balance`,`payment_remark`) VALUES 
 (3,2,1,'3000.00','20.00','120.00','tses remarkkkkkkkkkkkkkkkkkkkkklllkkkkkkkkkkk'),
 (4,2,1,'4000.00','30.00','220.00','aweaaaaaaaaaaaaaaaaaaaaaaaaaaaw'),
 (5,10,5,'3000.00','15.00','80.00','aetewafefsfwefszdcscsdv dfdf'),
 (6,11,6,'3400.00','20.00','340.00','kjkkjlkkkkkkkkkkkkkkkkkkkkkkkkkkkk uikhn k,jllllllllllllllljihiuhu');
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;


--
-- Definition of table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(25) NOT NULL,
  `usertype` varchar(15) NOT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`userid`,`username`,`password`,`usertype`) VALUES 
 (1,'Admin','1234','Admin'),
 (3,'User','2234','User');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
